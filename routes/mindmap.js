var express = require('express');
var router = express.Router();

var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    assert = require('assert'),
    validator = require('validator'),
    os = require('os');

var mongoose = require('mongoose');

var isNotValid = require('../helpers/validator').isNotValid;
var getDefaultValue = require('../helpers/validator').getDefaultValue;

var Mindmap = require('../models/mindmap').Mindmap;
var Category = require('../models/mindmap').Category;

var validateToken = require('../models/userToken').validateToken;


/*router.post('/getContentList', function (req, res, next) {

  var category = req.body.category; //optional
  var search_filter = req.body.search_filter;//optional
  var sort = req.body.sort;//optional

  var limit = getDefaultValue(req.body.limit, 10);
  var page = getDefaultValue(req.body.page, 1);

  var m = new Mindmap({'category':category});
  m.getContentList({
    'page':page,
    'limit':limit,
    'search_filter':search_filter,
    'sort':sort
  },function (err, results) {
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});*/

router.post('/getDetail', function (req, res, next) {

  var mindmap_id = req.body.mindmap_id;

  if ( isNotValid(mindmap_id) ) {
    next(10008);

  } else {
    console.log(req.body);

    var m = new Mindmap({'_id':mindmap_id});
    m.getDetail(function (err, results) {
      if (err) {
        next(err);
      } else {
        res.success(results);
      }
    });

  }

});

router.post('/getCategories', validateToken, function (req, res, next) {

  var master_user_id = req.body.master_user_id;

  if ( isNotValid(master_user_id) ) {
    next(10023);

  } else {

    var c = new Category({'master_user_id':master_user_id});
    c.getCategories({
      aToken:req.aToken
    },function (err, results) {
      if (err) {
        next(err);
      } else {
        res.success(results);
      }
    });

  }

});

router.post('/putReview', validateToken, function (req, res, next) {

  var mindmap_id = req.body.mindmap_id;
  var score = req.body.score;
  var title = req.body.title;
  var comment = req.body.comment;

  if ( isNotValid(mindmap_id) ) {
    next(10008);

  } else if ( isNotValid(score) ) {
    next(10013);

  } else if ( isNotValid(comment) ) {
    next(10014);

  } else {
    console.log(req.body);

    var m = new Mindmap({'_id':mindmap_id});
    m.putReview({
      'user_id': req.token_user_id,
      'score': score,
      'comment': comment,
      'title': title
    },function (err, results) {
      if (err) {
        next(err);
      } else {
        res.success(results);
      }
    });

  }

});

router.post('/getReviews', validateToken, function (req, res, next) {

  var mindmap_id = req.body.mindmap_id;

  var limit = getDefaultValue(req.body.limit, 10);
  var page = getDefaultValue(req.body.page, 1);

  if ( isNotValid(mindmap_id) ) {
    next(10008);

  } else {
    console.log(req.body);

    var m = new Mindmap({'_id':mindmap_id});
    m.getReviews({
      'page':page,
      'limit':limit
    },function (err, results) {
      if (err) {
        next(err);
      } else {
        res.success(results);
      }
    });

  }

});

router.post('/getMyReview', validateToken, function (req, res, next) {

  var mindmap_id = req.body.mindmap_id;

  if ( isNotValid(mindmap_id) ) {
    next(10008);

  } else {

    var m = new Mindmap({'_id':mindmap_id});
    m.getMyReview({
      'user_id':req.token_user_id
    },function (err, results) {
      if (err) {
        next(err);
      } else {
        res.success(results);
      }
    });
  }

});

router.post('/getMyReviews', validateToken, function (req, res, next) {

  var m = new Mindmap({'_id':req.token_user_id});
  m.getMyReviews(function (err, results) {
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});


module.exports = router;
