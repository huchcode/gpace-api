var express = require('express');
var router = express.Router();

var path = require('path');

var async = require('async');

var iap = require('in-app-purchase');
iap.config({
	googlePublicKeyPath: path.join(process.env.PWD, 'google_keys/')
});

var validateToken = require('../models/userToken').validateToken;

var isNotValid = require('../helpers/validator').isNotValid;

router.post('/validatePurchase', validateToken, function (req, res, next) {

	var productCode = req.body.product_code;
	var deviceType = req.body.device_type;

	if (isNotValid(productCode)) {
		next(50001);
	} else if (isNotValid(deviceType)) {
		next(50002);
	}

	async.waterfall([
		function (callback) {
			iap.setup(function (err) {
				if (err) {
					callback(50000);
				}

				if (deviceType == 'ios') {
					var receipt = req.body.receipt;

					iap.validate(iap.APPLE, receipt, function (err, appleRes) {
						if (err) {
							callback(52000);
						}

						if (iap.isValidated(appleRes)) {
							var purchaseDataList = iap.getPurchaseData(appleRes);
							var purchaseData = null;
							for (var i = 0; i < purchaseDataList.length; i++) {
								if (purchaseDataList[i].productId == productCode) {
									purchaseData = purchaseDataList[i];
								}
							}

							if (purchaseData) {
								callback(null, purchaseData);
							} else {
								callback(50003);
							}
						} else {
							callback(52000);
						}
					});
				} else if (deviceType == 'android') {
					var googleData = req.body.google_data;
					var googleSignature = req.body.google_signature;

					var googleReceipt = {
						data: googleData,
						signature: googleSignature
					}

					iap.validate(iap.GOOGLE, googleReceipt, function (err, googleRes) {
						if (err) {
							callback(53000);
						}

						if (iap.isValidated(googleRes)) {
							var purchaseDataList = iap.getPurchaseData(googleRes);
							var purchaseData = null;
							for (var i = 0; i < purchaseDataList.length; i++) {
								if (purchaseDataList[i].productId == productCode) {
									purchaseData = purchaseDataList[i];
								}
							}

							if (purchaseData) {
								callback(null, purchaseData);
							} else {
								callback(50003);
							}
						} else {
							callback(53000);
						}
					});
				} else {
					callback(50000);
				}
			});
		},
		function (purchase, callback) {
			callback(null, {});
		}
	], function (err, results) {
		if (err) {
			next(err);
		}

		res.success(results);
	});
});

module.exports = router;
