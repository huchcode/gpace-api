var express = require('express');
var router = express.Router();

var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    assert = require('assert'),
    validator = require('validator'),
    os = require('os');

var mongoose = require('mongoose');

var isNotValid = require('../helpers/validator').isNotValid;

var User = require('../models/user').User;
var QnA = require('../models/qna').QnA;
var validateToken = require('../models/userToken').validateToken;


router.post('/putQnA', validateToken, function(req, res, next) {

  var user_id = req.token_user_id;
  var aToken = req.aToken;

  var title = req.body.title;
  var upload_img_url = req.body.upload_img_url;
  var question = req.body.question;

  var currTimestamp = Math.floor(Date.now() / 1000);

  if ( isNotValid(question) ) {
    next(10006);

  } else {

    var datas = {
      "user": user_id,
      "question": {
        "question": question,
        "title": title,
        "upload_img_url": upload_img_url,
        "created": currTimestamp
      },
      "answer":{}
    };
    var qna = new QnA(datas);

    // Submit to the DB
    qna.save(function (err) {
      if (err) {
        next(99996);
      } else {
        res.success({ 'QnA':qna, 'aToken':aToken });
      }
    });

  }

});

router.post('/getQnAList', validateToken, function (req, res, next) {

  var user_id = req.token_user_id;
  var aToken = req.aToken;

  var qna = new QnA({ 'user': user_id });
  qna.getQuestions({
    'aToken':aToken
  },function (err, results) {
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});

router.post('/getQnA', validateToken, function (req, res, next) {

  var qna_id = req.body.qna_id;

  var user_id = req.token_user_id;
  var aToken = req.aToken;

  if ( isNotValid(qna_id) ) {
    next(10011);

  } else {
    var qna = new QnA({
        '_id': qna_id,
        'user': user_id
    });
    qna.getQuestionDetails({
      'aToken':aToken
    },function (err, result) {
      if (err) {
        next(err);
      } else {
        res.success(result);
      }
    });
  }
});


module.exports = router;
