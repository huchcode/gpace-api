var express = require('express');
var router = express.Router();

var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    os = require('os');

var mongoose = require('mongoose');

var networkInterfacesObj = os.networkInterfaces();

if(networkInterfacesObj.enp0s25) {
  var localIPAddress = networkInterfacesObj.enp0s25[0].address;
} else if(networkInterfacesObj.eth0) {
  var localIPAddress = networkInterfacesObj.eth0[0].address;
}

if (localIPAddress == '192.168.0.112') {
  var devserver = true;
} else {
  var devserver = false;
}

if (devserver) {
  var mongooseConnectionString = 'mongodb://huchNormal:kitiwit7@192.168.0.112:27017/api';
} else {
  var mongooseConnectionString = 'mongodb://gpac:wlvorakzptvmffpdltm2016@115.68.185.122:27017/gpace';
}

var options = { server: { socketOptions: { keepAlive: 1 } } };
var mongoconn = mongoose.connect(mongooseConnectionString,options);
//console.log(mongoconn);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  // yay!
});

require('../models/errors');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'GPACE API' });
});

router.get('/testform', function(req, res) {
  res.render('testform', { title: 'GPACE Test Form' });
});


module.exports = router;