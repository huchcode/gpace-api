var express = require('express');
var router = express.Router();

var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    assert = require('assert'),
    validator = require('validator'),
    os = require('os'),
    sha1 = require('sha1'),
    sha3 = require('js-sha3');

var mongoose = require('mongoose');

var isNotValid = require('../helpers/validator').isNotValid;

var User = require('../models/user').User;
var Mylibrary = require('../models/user').Mylibrary;
var Library = require('../models/user').Library;
var Buying = require('../models/user').Buying;
var Buyer = require('../models/user').Buyer;
var UserDownloads = require('../models/user').UserDownloads;

var validateToken = require('../models/userToken').validateToken;

router.post('/signIn', function(req, res, next) {

  var device_id = req.body.device_id;
  var email = req.body.email;
  var password = req.body.password;

  if (email == undefined || email == null || email == "") {
    next(10001);
  } else if (password == undefined || password == null || password == "") {
    next(10002);
  } else if (device_id == undefined || device_id == null || device_id == "") {
    next(10015);
  }


  var salt = "";
  var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

  for( var i=0; i < 6; i++ ) {
    salt += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  var aExpireTimestamp = ( (Math.floor(Date.now() / 1000)) + (1 * 60 * 60) );
  var rExpireTimestamp = ( (Math.floor(Date.now() / 1000)) + (7 * 24 * 60 * 60) );

  var aToken = sha1(email+aExpireTimestamp+salt);
  var rToken = sha3.sha3_256(email+rExpireTimestamp+salt);

  var user = new User({'email': email, 'password': password});
  user.signIn({"aToken": aToken,"rToken": rToken},function (err, results) {
    console.log('SignIn results',results);
    if (err) {
      next(err);
    } else {

      mongoose.model('UserToken').update({
        "user_id": results.user._id
      },{
        "user_id": results.user._id,
        "device_id": device_id,
        "aToken": aToken,
        "aExpire": aExpireTimestamp,
        "rToken": rToken,
        "rExpire": rExpireTimestamp
      },{
        upsert:true
      },function(err, response) {}
      );

      res.success(results);
    }
  });

});

router.post('/getProfile', validateToken, function (req, res, next) {

  var user = new User({'_id':req.token_user_id});
  user.getUserInfo({
    aToken:req.aToken
  },function (err, results){
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});

router.post('/getUserDownloads', validateToken, function (req, res, next) {

  var ud = new UserDownloads({'user_id':req.token_user_id});
  ud.getUserDownloads({
    aToken:req.aToken
  },function (err, results){
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});

router.post('/getAllBooks', validateToken, function (req, res, next) {
  var search = req.body.search;

  var mylib = new Mylibrary({'user_id':req.token_user_id});
  mylib.getAllBooks({
    aToken:req.aToken,
    search:search
  },function (err, results){
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});

router.post('/getAllPrivateBooks', validateToken, function (req, res, next) {

  var mylib = new Mylibrary({'user_id':req.token_user_id});
  mylib.getAllPrivateBooks({
    aToken:req.aToken
  },function (err, results){
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});

router.post('/getPrivateBooksFiltered', validateToken, function (req, res, next) {
  var filter_type = req.body.filter_type;

  if ( isNotValid(filter_type) ) {
      next(10022);

  } else {
    var mylib = new Mylibrary({'user_id':req.token_user_id});
    mylib.getPrivateBooksFiltered({
      aToken:req.aToken,
      filter_type:filter_type
    },function (err, results){
      if (err) {
        next(err);
      } else {
        res.success(results);
      }
    });
  }

});

router.post('/getAllPublicBooks', validateToken, function (req, res, next) {

  var mylib = new Mylibrary({'user_id':req.token_user_id});
  mylib.getAllPublicBooks({
    aToken:req.aToken
  },function (err, results){
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});

router.post('/getPublicBooksCategory', validateToken, function (req, res, next) {
  var category_id = req.body.category_id;

  if ( isNotValid(category_id) ) {
      next(10021);

  } else {
    var mylib = new Mylibrary({'user_id':req.token_user_id});
    mylib.getMyLibrariesCategory({
      aToken:req.aToken,
      category_id:parseInt(category_id)
    },function (err, results){
      if (err) {
        next(err);
      } else {
        res.success(results);
      }
    });
  }

});

router.post('/getDownload', validateToken, function (req, res, next) {

  var user_id = req.token_user_id;
  var book_id = req.body.book_id;

  if ( isNotValid(user_id) ) {
    next(10004);

  } else if ( isNotValid(book_id) ) {
    next(10009);

  } else {

    mongoose.model('Mindmap').findOne({
      'book_id':book_id
    },function(err, mindmapObj) {
      if (err) {
        next(11003);
      } else {
        if(mindmapObj) {
          //console.log('mindmap resp',mindmapObj);

          var mylib = new Mylibrary({'user_id':user_id});
          mylib.getDownload({
            'aToken': req.aToken,
            'book_id': book_id,
            'version': mindmapObj.version
          },function (err, results){
            if (err) {
              next(err);
            } else {
              res.success(results);
            }
          });
        } else {
          next(11003);

        }
      }
    });

  }

});

module.exports = router;
