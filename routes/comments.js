var express = require('express');
var router = express.Router();

var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    assert = require('assert'),
    validator = require('validator'),
    os = require('os');

var mongoose = require('mongoose');

var isNotValid = require('../helpers/validator').isNotValid;
var getDefaultValue = require('../helpers/validator').getDefaultValue;

var User = require('../models/user').User;
var Comments = require('../models/comments').Comments;
var AnswerComment = require('../models/comments').AnswerComment;
var validateToken = require('../models/userToken').validateToken;


router.post('/addComment', validateToken, function (req, res, next) {

  var content_id = req.body.content_id;
  var comment = req.body.comment;
  var link = req.body.link;

  var user_id = req.token_user_id;
  var aToken = req.aToken;

  var timestamp = Math.floor(Date.now() / 1000);

  if ( isNotValid(link) ) {
    var link = "";
  }

  if ( isNotValid(content_id) ) {
    next(11002);

  } else if ( isNotValid(comment) ) {
    next(11001);

  } else {

    var Comments = mongoose.model('Comments');
    var datas = {
      "user" : user_id,
      "content_id" : content_id,
      "comment" : comment,
      "link" : link,
      "timestamp" : timestamp
    };
    var comments = new Comments(datas);

    // Submit to the DB
    comments.save(function (err) {
      if (err) {
        next(99996);
      } else {
        res.success({ 'comment':datas, 'aToken':aToken });
      }
    });


  }

});

router.post('/getComments', validateToken, function (req, res, next) {

  var content_id = req.body.content_id;
  var show_all = req.body.show_all;

  var user_id = req.token_user_id;
  var aToken = req.aToken;

  var limit = getDefaultValue(req.body.limit, 10);
  var page = getDefaultValue(req.body.page, 1);
  var sort = 'timestamp';
  var order = 'descending';

  var comments = new Comments({
    'user':user_id,
    'content_id':content_id
  });
  comments.getComments({
    'aToken':aToken,
    'show_all':show_all,
    'limit':limit,
    'page':page,
    'sort':sort,
    'order':order
  }, function (err, results) {
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});

router.post('/deleteComment', validateToken, function (req, res, next) {

  var comment_id = req.body.comment_id;
  var aToken = req.aToken;

  var comments = new Comments({'_id':comment_id});
  comments.deleteComment({
    'aToken':aToken
  },function (err, results) {
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});



router.post('/addAnswerComment', validateToken, function (req, res, next) {

  var comment_id = req.body.comment_id;
  var comment_user_id = req.body.comment_user_id;
  var answer = req.body.answer;

  var user_id = req.token_user_id;
  var aToken = req.aToken;

  var currTimestamp = Math.floor(Date.now() / 1000);

  if ( isNotValid(comment_id) ) {
    next(10018);

  } else if ( isNotValid(answer) ) {
    next(10019);

  } else {
    var datas = {
      "comment": comment_id,
      "comment_user_id": comment_user_id,
      "user": user_id,
      "answer": answer,
      "created": currTimestamp,
      "read": 0,
    };
    var ac = new AnswerComment(datas);

    // Submit to the DB
    ac.save(function (err) {
      if (err) {
        next(99996);
      } else {
        res.success({'answerComment':ac,'aToken':aToken});
      }
    });

  }

});

router.post('/getAnswerComments', validateToken, function (req, res, next) {

  var comment_id = req.body.comment_id;

  var user_id = req.token_user_id;
  var aToken = req.aToken;

  var ac = new AnswerComment({
    'comment':comment_id
  });
  ac.getAnswerComments({
    'aToken':aToken
  }, function (err, results) {
    if (err) {
      next(err);
    } else {

      mongoose.model('Comments').findOne({"_id":comment_id}).exec(function(err, commentsObj){
        if (commentsObj) {
          var currTimestamp = Math.floor(Date.now() / 1000);
          if (commentsObj.user == user_id) {
            mongoose.model('AnswerComment').update({
              'comment':commentsObj._id
            },{
              'read':currTimestamp
            },function(err, response) {});
          }
        }
      });


      res.success(results);
    }
  });

});

router.post('/deleteAnswerComment', validateToken, function (req, res, next) {

  var answer_id = req.body.answer_id;

  var user_id = req.token_user_id;
  var aToken = req.aToken;

  var ac = new AnswerComment({'_id':answer_id,'user':user_id});
  ac.deleteAnswerComment({
    'aToken':aToken
  },function (err, results) {
    if (err) {
      next(err);
    } else {
      res.success(results);
    }
  });

});

module.exports = router;
