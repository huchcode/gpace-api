function pad (str, max) {
	str = str.toString();
	return str.length < max ? pad('0' + str, max) : str;
}

function getWeekDates () {
	var weeks = [];
	var today = new Date();
	for (var i = 0; i < 7; ++i) {
		var day = 1000 * 60 * 60 * 24;
		var d = new Date(today - day * i);
		weeks[i] = 'day:' + d.getFullYear() + pad(d.getMonth() + 1, 2) + pad(d.getDate(), 2);
	}
	return weeks;
}

function today () {
	var d = new Date();
	return 'day:' + d.getFullYear() + pad(d.getMonth() + 1, 2) + pad(d.getDate(), 2);
}

module.exports = {
	getWeekDates: getWeekDates,
	today: today
}