var validator = require('validator');

function isNotValid (value) {
  value = String(value);
  return value == "" || value == undefined || validator.isNull(value);
}

function getDefaultValue (value, defaultValue) {
	if (isNotValid(value)) {
		return defaultValue;
	}
	return value;
}

module.exports = {
	isNotValid: isNotValid,
	getDefaultValue: getDefaultValue
};