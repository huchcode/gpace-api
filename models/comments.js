var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator');

var commentsSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  content_id: Number,
  comment: String,
  link: String,
  timestamp: Number
}, { collection: 'comments', versionKey: false });

var answerCommentSchema = new Schema({
  comment : { type: Schema.Types.ObjectId, ref: 'Comments' },
  comment_user_id: { type: Schema.Types.ObjectId, ref: 'User' },
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  answer: String,
  created: Number,
  read: Number
}, { collection: 'answerComment', versionKey: false });

commentsSchema.methods.getComments = function(p,callback) {
  var data = {};
  var err = null;

  var limitperpage = p.limit;
  var pagenumber = p.page;
  var sortwhat = p.sort;
  var sortorder = p.order;

  var show_all = p.show_all;
  var aToken = p.aToken;

  var content_id = this.content_id;
  var user_id = this.user;

  var startwith = (pagenumber * limitperpage) - limitperpage;

  console.log('getcomments params',p,this);

  var query;
  if (show_all==undefined || show_all==null || show_all=="" || show_all=="0" || show_all=="false" || show_all==false) {
    query = {'user': user_id,'content_id': content_id};
  } else  {
    query = {'content_id': content_id}
  }

  this.model('Comments').find(query).populate({
    path: 'user',
    select: 'username name email profile_picture'
  }).skip(startwith).limit(limitperpage).sort(
    [[ sortwhat, sortorder]]
  ).exec(function(err, comments){
    if(err){
      err = 99997;
    } else if(comments){
      data = { "Comments" : comments, "aToken": aToken };
    } else {
      err = 99998;
    }
    return callback(err,data);
  });

};

commentsSchema.methods.deleteComment = function(p,callback) {
  var data = {};
  var err = null;

  var aToken = p.aToken;
  var comment_id = this._id;

  console.log('deleteComment params',p,this);

  if (comment_id==undefined || comment_id==null || comment_id==""){
    err = 11004;
  } else {
    this.model('Comments').findByIdAndRemove({
      '_id':comment_id
    }).exec(function(err, comment){
      if(err){
        err = 99995;
      } else if(comment){
        data = {"comment_id":comment_id, "aToken":aToken};
      } else {
        err = 99998;
      }
      return callback(err,data);
    });
  }
};

answerCommentSchema.methods.getAnswerComments = function(p,callback) {
  var data = {};
  var err = null;

  var aToken = p.aToken;
  var comment_id = this.comment;

  console.log('getAnswerComments params',p,this);

  this.model('AnswerComment').find({
    'comment': comment_id
  }).populate({
    path: 'user',
    select: 'username name email profile_picture'
  }).exec(function(err, acObj){
    if(err){
      err = 99995;
    } else if(acObj){
      data = { "AnswerComments" : acObj, "aToken": aToken };
    } else {
      err = 99998;
    }
    return callback(err,data);
  });

};

answerCommentSchema.methods.deleteAnswerComment = function(p,callback) {
  var data = {};
  var err = null;

  var aToken = p.aToken;
  var answer_id = this._id;

  console.log('deleteAnswerComment params',p,this);

  if (answer_id==undefined || answer_id==null || answer_id==""){
    err = 10020;
  } else {
    this.model('AnswerComment').findByIdAndRemove({
      '_id':answer_id
    }).exec(function(err, acObj){
      if(err){
        err = 99995;
      } else if(acObj){
        data = {"answer_id":answer_id, "aToken":aToken};
      } else {
        err = 99998;
      }
      return callback(err,data);
    });
  }
};

var Comments = mongoose.model('Comments', commentsSchema);
var AnswerComment = mongoose.model('AnswerComment', answerCommentSchema);

module.exports = {
  Comments: Comments,
  AnswerComment: AnswerComment,
};
