var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var validator = require('validator');
var sha1 = require('sha1');

var async = require('async');

var userTokenSchema = new Schema({
  user_id: String,
  device_id: String,
  aToken: String,
  aExpire: Number,
  rToken: String,
  rExpire: Number
}, {
  collection: 'userToken',
  versionKey: false
});

userTokenSchema.methods.getAccessTokenDetails = function(callback) {

  q = this.model('UserToken').findOne({
        'user_id': this.user_id,
        'device_id': this.device_id,
        'aToken': this.aToken,
        'rToken': this.rToken
      });
  q.exec(function(err, resp){
    return callback(resp);
  });

};

userTokenSchema.statics = {
  getAccessTokenDetails: function (params, callback) {
    return this.findOne({
      'user_id': params.user_id,
      'device_id': params.device_id,
      'aToken': params.aToken,
      'rToken': params.rToken
    }, callback);
  }
};

var UserToken = mongoose.model('UserToken', userTokenSchema);

function validateToken (req, res, next) {
  var user_id = req.body.user_id;
  var device_id = req.body.device_id;
  var aToken = req.body.aToken;
  var rToken = req.body.rToken;

  var currTimestamp = Math.floor(Date.now() / 1000);


  if (aToken == undefined) {
    next(10016);
  } else if (rToken == undefined) {
    next(10017);
  } else if (user_id == undefined) {
    next(10004);
  } else if (device_id == undefined) {
    next(10015);
  }

  UserToken.getAccessTokenDetails({
    'user_id': user_id,
    'device_id': device_id,
    'aToken': aToken,
    'rToken': rToken
  }, function (err, result) {
    if (result) {
      if (result.rExpire < currTimestamp) {
        next(12007);
      } else if (result.aExpire > currTimestamp) {

        req.token_user_id = result.user_id;
        req.aToken = result.aToken;
        req.rToken = result.rToken;
        next();

      } else if (result.aExpire < currTimestamp) {

        var aExpireTimestamp = ( (Math.floor(Date.now() / 1000)) + (1 * 60 * 60) );

        var salt = "";
        var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 6; i++ ) {
          salt += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        var aToken = sha1(result.user_id+aExpireTimestamp+salt);
        //TODO: reset aExpire and aToken
        mongoose.model('UserToken').update({
          "user_id": result.user_id
        },{
          "aToken": aToken,
          "aExpire": aExpireTimestamp
        },{
          upsert:true
        },function(err, response) {}
        );

        req.token_user_id = result.user_id;
        req.aToken = aToken;
        req.rToken = result.rToken;
        next();

      } else {
        next(11001);
      }
    } else {
      // invalid access token
      next(11001);
    }
  });
}

module.exports = {
  UserToken: UserToken,
  validateToken: validateToken
};
