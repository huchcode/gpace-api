var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var isNotValid = require('../helpers/validator').isNotValid;
var validator = require('validator');
var sha3 = require('js-sha3');
var sha1 = require('sha1');

var UserToken = require('./userToken').UserToken;

var userSchema = new Schema({
  username: String,
  email: String,
  password: String,
  name: String,
  status: String,
  department_id: Number,
  position_id: Number,
  role: String,
  master_user_id : Number,
  changepwd : Number, //timestamp
  created: Number, //timestamp
  modified: Number, //timestamp
  user_id: Number
}, { collection: 'user', versionKey: false });


var mylibrarySchema = new Schema({
  user_id: { type: Schema.Types.ObjectId, ref: 'User' },
  library: [librarySchema]
}, { collection: 'mylibrary', versionKey: false });

var librarySchema = new Schema({
  mindmap_id : { type: Schema.Types.ObjectId, ref: 'Mindmap' },
  book_id: Number,
  version: String,
  staff_id: Number, //user_id
  department_id: Number,
  position_id: Number,
  public_id: Number, //master_user_id
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  },
  'id':false
});


var buyingSchema = new Schema({
  book_id: Number,
  buyers: [buyersSchema]
}, { collection: 'buying', versionKey: false });

var buyersSchema = new Schema({
  user_id : { type: Schema.Types.ObjectId, ref: 'User' },
  price: String,
  receipt: String,
  created: Number
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  },
  'id':false
});


var userDownloadsSchema = new Schema({
  user_id : { type: Schema.Types.ObjectId, ref: 'User' },
  book_id: Number,
  date: Number
}, { collection: 'userDownloads', versionKey: false });


userSchema.methods.checkDuplicate = function(callback) {

  var data = {};
  var err = null;

  var email = this.email;

  this.model('User').findOne({ 'email': email }).exec(function(err, userObj){
    if(err){
      err = 99997;
    } else if(userObj){
      data = { "user" : userObj };
    } else {
      err = 99998;
    }
    return callback(err, data);
  });
};

userSchema.methods.signIn = function(p,callback) {

  var data = {};
  var err = null;

  console.log('signIn params',this);

  var email = this.email;
  var password = this.password;
  var device_id = this.device_id;
  var aToken = p.aToken;
  var rToken = p.rToken;

  var currTimestamp = Math.floor(Date.now() / 1000);

  var query = {};

  query = {'email': email};

  this.model('User').findOne(query).exec(function(err, userObj){

    if(err){
      err = 99997;
    } else if(userObj){

      if( userObj.password != sha1(password)) {
        err = 12005;
      } else {
        var respObj = userObj;
        respObj.password = '*****';
        delete respObj.password;
        //console.log('respObj1',err,respObj);

        data = { "user" : respObj, "aToken": aToken, "rToken": rToken};

      }

    } else {
      err = 12004;
    }
    return callback(err, data);
  });

};

userDownloadsSchema.methods.getUserDownloads = function(p,callback) {

  var data = {};
  var err = null;

  console.log('getUserDownloads params',p,this);

  mongoose.model('UserDownloads').find({
    'user_id': this.user_id
  }).exec(function(err, userDownObj){
    //console.log('userDownloadsObj',err,userDownObj);

    if(err){
      err = 99997;
    } else if(userDownObj){
      data = { "userDownloads":userDownObj, "aToken": p.aToken };

    } else {
      err = 12004;
    }
    return callback(err, data);

  });
};

userSchema.methods.getUserInfo = function(p,callback) {

  var data = {};
  var err = null;

  console.log('getUserInfo params',this);

  var query = {'_id': this._id};
  var filters = {'password':0,'changepwd':0};

  this.model('User').findOne(query, filters).exec(function(err, userObj){
    if(err){
      err = 99997;
    } else if(userObj){
      data = { "user" : userObj, 'aToken':p.aToken };
    } else {
      err = 99998;
    }
    return callback(err, data);
  });
};

userSchema.methods.resetPassword = function(callback) {

  var data = {};
  var err = null;

  console.log('resetPassword params',this);

  var newpassword = "";
  var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

  for( var i=0; i < 6; i++ ) {
    newpassword += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  var query = {'email':this.email};
  var update = {'password':sha3.sha3_256(newpassword)};
  var option = {};

  this.model('User').update(query,update,option).exec(function(err, userObj){
    if(err){
      err = 99995;
    } else if(userObj){

      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'noreply@huchcode.com',
        to: this.email,
        subject: 'GPAC 새 비밀번호',
        text: 'GPAC  비밀번호는 '+ newpassword +'입니다.'
      });

      data = {
        "user": {
          "password":newpassword
        }
      };

    } else {
      err = 99998;
    }

    return callback(err, data);
  });
};

mylibrarySchema.methods.getMyLibrary = function(p,callback) {

  var data = {};
  var err = null;

  console.log('getMyLibrary params',p,this);

  var book_id = parseInt(p.book_id);

  mongoose.model('Mylibrary').findOne({
    'user_id': this.user_id,
    'library.book_id': book_id
  }).populate({
    path: 'library.mindmap_id',
    model: 'Mindmap',
    select: 'book_id title price size description iconurl version'
  }).exec(function(err, returnObj){
    if(err){
      err = 99997;
    } else if(returnObj){



      var library ;
      for(i = 0; i < returnObj.library.length; i++) {
        if (String(returnObj.library[i].book_id) == String(book_id)) {
          library = returnObj.library[i];
        }
      }

      data = library;
    } else {
      err = 99998;
    }
    return callback(err, data);
  });
};

mylibrarySchema.methods.getAllBooks = function(p,callback) {

  var data = {};
  var err = null;

  console.log('getAllBooks params',p,this);

  mongoose.model('Mylibrary').findOne({
    'user_id': this.user_id
  }).populate({
    path: 'library.mindmap_id',
    model: 'Mindmap',
    select: 'book_id title price size description iconurl version category_name author_name'
  }).exec(function(err, respObj){
    if(err){
      err = 99997;
    } else if(respObj){
      if (p.search!=null && p.search!=undefined && p.search!="") {
        var userlib = respObj.library;
        var userlibresp = [];
        for (key in userlib) {
          if (String(parseInt(key, 10)) === key && userlib.hasOwnProperty(key)) {
            var mindmapObj = userlib[key].mindmap_id;
            var mindmapString = JSON.stringify(mindmapObj);
            if (mindmapString.indexOf(p.search)>0) {
              userlibresp.push(userlib[key]);
            }
          }
        }
      } else {
        var userlibresp = respObj.library;
      }

      data = {"myLibrary":userlibresp, 'aToken':p.aToken};
    } else {
      err = 99998;
    }
    return callback(err, data);
  });
};

mylibrarySchema.methods.getAllPrivateBooks = function(p,callback) {

  var data = {};
  var err = null;

  console.log('getAllPrivateBooks params',p,this);

  mongoose.model('Mylibrary').findOne({
    'user_id': this.user_id
  }).populate({
    path: 'library.mindmap_id',
    model: 'Mindmap',
    select: 'book_id title price size description iconurl version category_name author_name'
  }).exec(function(err, respObj){
    if(err){
      err = 99997;
    } else if(respObj){
      var userlib = respObj.library;
      var userlibresp = [];
      for (key in userlib) {
        if (String(parseInt(key, 10)) === key && userlib.hasOwnProperty(key)) {
          if (userlib[key].public_id == 0) {
            userlibresp.push(userlib[key]);
          }
        }
      }

      data = {"myLibrary":userlibresp, 'aToken':p.aToken};
    } else {
      err = 99998;
    }
    return callback(err, data);
  });

};

mylibrarySchema.methods.getPrivateBooksFiltered = function(p,callback) {

  var data = {};
  var err = null;

  var filter_type = p.filter_type;

  console.log('getPrivateBooksFiltered params',p,this);

  mongoose.model('Mylibrary').findOne({
    'user_id': this.user_id
  }).populate({
    path: 'library.mindmap_id',
    model: 'Mindmap',
    select: 'book_id title price size description iconurl version category_name author_name'
  }).exec(function(err, respObj){
    if(err){
      err = 99997;
    } else if(respObj){
      var userlib = respObj.library;
      var userlibresp = [];
      for (key in userlib) {
        if (String(parseInt(key, 10)) === key && userlib.hasOwnProperty(key)) {
          if (filter_type=='staff') {
            if (userlib[key].staff_id > 0) {
              userlibresp.push(userlib[key]);
            }
          } else if (filter_type=='department') {
            if (userlib[key].department_id > 0) {
              userlibresp.push(userlib[key]);
            }
          } else if (filter_type=='position') {
            if (userlib[key].position_id > 0) {
              userlibresp.push(userlib[key]);
            }
          } else {
            err = 11004;
          }
        }
      }
      if (err==null) {
        data = {"myLibrary":userlibresp, 'aToken':p.aToken};
      }
    } else {
      err = 99998;
    }
    return callback(err, data);
  });

};

mylibrarySchema.methods.getAllPublicBooks = function(p,callback) {

  var data = {};
  var err = null;

  console.log('getAllPublicBooks params',p,this);

  mongoose.model('Mylibrary').findOne({
    'user_id': this.user_id
  }).populate({
    path: 'library.mindmap_id',
    model: 'Mindmap',
    select: 'book_id title price size description iconurl version category_name author_name'
  }).exec(function(err, respObj){
    if(err){
      err = 99997;
    } else if(respObj){
      var userlib = respObj.library;
      var userlibresp = [];
      for (key in userlib) {
        if (String(parseInt(key, 10)) === key && userlib.hasOwnProperty(key)) {
          if (userlib[key].public_id > 0) {
            userlibresp.push(userlib[key]);
          }
        }
      }

      data = {"myLibrary":userlibresp, 'aToken':p.aToken};
    } else {
      err = 99998;
    }
    return callback(err, data);
  });

};

mylibrarySchema.methods.getMyLibrariesCategory = function(p,callback) {

  var data = {};
  var err = null;

  var category_id = p.category_id;

  console.log('getMyLibrariesCategory params',p,this);

  mongoose.model('Mylibrary').findOne({
    'user_id': this.user_id,
    'library.category_id': category_id
  }).populate({
    path: 'library.mindmap_id',
    model: 'Mindmap',
    select: 'book_id title price size description iconurl version category_name author_name'
  }).exec(function(err, respObj){
    if(err){
      err = 99997;
    } else if(respObj){
      var userlib = respObj.library;
      var userlibresp = [];
      for (key in userlib) {
        if (String(parseInt(key, 10)) === key && userlib.hasOwnProperty(key)) {
          if (userlib[key].category_id === category_id && userlib[key].public_id > 0) {
            //console.log('delete userlib[key]',userlib[key]);
            userlibresp.push(userlib[key]);
          }
        }
      }

      data = {"myLibrary":userlibresp, 'aToken':p.aToken};
    } else {
      err = 99998;
    }
    return callback(err, data);
  });

};

mylibrarySchema.methods.getDownload = function(p,callback) {

  var data = {};
  var err = null;

  var aToken = p.aToken;
  var book_version = p.version;
  var bookId = p.book_id;
  var user_id = this.user_id;

  var currTimestamp = Math.floor(Date.now() / 1000);

  console.log('getDownload params',p,this);

  Mylibrary.find({
    'user_id': user_id,
    'library.book_id': parseInt(bookId)
  }).exec(function(err, respObj){
    //console.log('respObj',respObj);
    if(err){
      err = 99997;

    } else if(respObj){

      //console.log('getDownload resp',respObj);

      var jsontxt = "/zip/"+bookId+"_"+book_version+".txt";
      var zipfile = "/zip/"+bookId+"_"+book_version+".zip";
      data = { "user_id":user_id , "json" : jsontxt, "zip" : zipfile, "aToken": aToken };

      mongoose.model('UserDownloads').update({'book_id':bookId,'user_id':user_id},{'date':currTimestamp},{upsert: true}).exec(function(err, userObj){});

    } else {
      err = 99998;

    }
    return callback(err, data);
  });
};

buyingSchema.methods.putBuying = function(p,callback) {

  var data = {};
  var err = null;

  var book_id = this.book_id;
  var price = p.price;
  var receipt = p.receipt;
  var user_id = p.user_id;

  var currTimestamp = Math.floor(Date.now() / 1000);

  console.log('putBuying params',p,this);

  Buying.findOne({
    'book_id': book_id
  }).exec(function(err, buyingObj){

    if(err){
      err = 99997;

    } else if(buyingObj){

      var duplicate = false;
      for(i = 0; i < buyingObj.buyers.length; i++) {
        if (buyingObj.buyers[i].user_id == user_id) {
          duplicate = true;
        }
      }

      if (duplicate) {
        err = 12009;
      } else {
        var buyerData = new Buyer({
          'user_id': user_id,
          'price': price,
          'receipt': receipt,
          'created': currTimestamp
        });

        buyingObj.buyers.push(buyerData);
        buyingObj.save();

        mongoose.model('Mindmap').update({
          'book_id':book_id
        },{
          '$inc': {'meta.downloads' : 1}
        },function(err, response) {});

        callback(null, buyingObj);
      }

    } else {
      err = 99998;

    }
    return callback(err, data);
  });
};

mylibrarySchema.methods.putMyLibrary = function(p,callback) {

  var data = {};
  var err = null;

  var user_id = this.user_id;
  var mindmap_id = p.mindmap_id;
  var book_id = p.book_id;
  var version = p.version;

  console.log('putMyLibrary params',p,this);

  Mylibrary.findOne({
    'user_id': user_id
  }).exec(function(err, libObj){

    if(err){
      err = 99997;

    } else if(libObj){

      var duplicate = false;
      for(i = 0; i < libObj.library.length; i++) {
        if (libObj.library[i].book_id == book_id) {
          duplicate = true;
        }
      }

      if (duplicate) {
        err = 12009;
      } else {
        var libraryData = new Library({
          'mindmap_id': mindmap_id,
          'book_id': book_id,
          'version': version
        });

        libObj.library.push(libraryData);
        libObj.save();
        callback(null, libObj);
      }

    } else {
      err = 99998;

    }
    return callback(err, data);
  });
};

var User = mongoose.model('User', userSchema);
var Mylibrary = mongoose.model('Mylibrary', mylibrarySchema);
var Library = mongoose.model('Library', librarySchema);
var Buying = mongoose.model('Buying', buyingSchema);
var Buyer = mongoose.model('Buyer', buyersSchema);
var UserDownloads = mongoose.model('UserDownloads', userDownloadsSchema);

module.exports = {
  User: User,
  Mylibrary: Mylibrary,
  Library: Library,
  Buying: Buying,
  Buyer: Buyer,
  UserDownloads: UserDownloads,
};
