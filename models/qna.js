var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator'),
  async = require('async'),
  mongodb = require('mongodb');

var qnaSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  question: {
    question: String,
    title: String,
    upload_img_url: String,
    created: Number
  },
  answer: {
    answer: String,
    created: Number
  }
}, { collection: 'qna', versionKey: false });


qnaSchema.methods = {
  getQuestions: function (p,callback) {
    var data = {};

    var aToken = p.aToken;

    console.log('getQuestions params',this);

    var q;
    q = this.model('QnA').find({
      'user': this.user
    }).populate({
      path: 'user',
      select: 'username name email profile_picture'
    });

    //q.sort([[ sortwhat, sortorder]]);
    q.exec(function(err, resp){
      if (err) {
        err = 99997;
      } else if (resp) {
        data = {
          "qna": resp,
          "aToken": aToken
        };
      } else {
        err = 99998;
      }

      return callback(err, data);
    });
  },
  getQuestionDetails: function (p,callback) {
    var data = {};

    var aToken = p.aToken;

    console.log('getQuestionDetails params',this);

    var q;
    q = this.model('QnA').findOne({
      '_id': this._id
    }).populate({
      path: 'user',
      select: 'username name email profile_picture'
    });
    q.exec(function(err, resp){
      if (err) {
        err = 99997;
      } else if (resp) {
        data = {
          "qna": resp,
          "aToken": aToken
        };
      } else {
        err = 99998;
      }

      return callback(err, data);
    });
  }
};

var QnA = mongoose.model('QnA', qnaSchema);

module.exports = {
  QnA: QnA
};
