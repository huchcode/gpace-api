var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator'),
  async = require('async'),
  mongodb = require('mongodb'),
  ObjectID = require('mongodb').ObjectID;

var isNotValid = require('../helpers/validator').isNotValid;

var mindmapSchema = new Schema({
  book_id: String,
  title: String,
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  author_name: String,
  description: String,
  category_id: Number,
  category_name: String,
  language: String,
  price: String,
  price_code: String,
  iconurl: String,
  sshot: Schema.Types.Mixed,
  size: String,
  version: Number,
  release: Number,
  updated: Number,
  review_score: {type: Number, default: 0},
  review_count: {type: Number, default: 0},
  review_count_1: {type: Number, default: 0},
  review_count_2: {type: Number, default: 0},
  review_count_3: {type: Number, default: 0},
  review_count_4: {type: Number, default: 0},
  review_count_5: {type: Number, default: 0},
  reviews: [mindmapReviewSchema],
  active: {type: Number, default: 1},
  meta : {
    downloads : {type: Number, default: 0},
    updates : {type: Number, default: 0}
  }
}, { collection: 'mindmap', versionKey: false });

var mindmapReviewSchema = new Schema({
  user_id : { type: Schema.Types.ObjectId, ref: 'User' },
  score: {type: Number, default: 0},
  title: String,
  comment: String,
  created: Number
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  },
  'id':false
});

var categorySchema = new Schema({
  master_user_id: Number,
  categories: [categoriesSchema],
}, { collection: 'category', versionKey: false });

var categoriesSchema = new Schema({
  category_id: String,
  category_name: Number
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  },
  'id':false
});


mindmapSchema.methods.getContentList = function(p,callback) {

  var data = {};

  var limitperpage = p.limit;
  var pagenumber = p.page > 0 ? p.page : 1;
  var startwith = (pagenumber * limitperpage) - limitperpage;

  var search_filter = p.search_filter;

  var currTimestamp = Math.floor(Date.now() / 1000);

  console.log('getContentList params',p,this);

  var query = {};

  query.active = 1;

  if (this.category) {
    query.category = this.category;
  }

  var filters = {
    reviews:{$slice:-2}
  };

  console.log('getContentList query',query);

  var q;
  q = this.model('Mindmap').find(query,filters);

  if (p.search_filter) {
    q.or([
        { 'title': { "$regex": p.search_filter, "$options": 'i' } },
        { 'description': { "$regex": p.search_filter, "$options": 'i' } }
      ]);
  }

  async.parallel({
    total: function (callback) {
      q.exec(function (err, results) {
        callback(null, results.length);
      });
    },
    data: function (callback) {

      q.skip(startwith);
      q.populate({
        path: 'author',
        model: 'User',
        select: 'nickname'
      });
      q.populate({
        path: 'reviews.user_id',
        model: 'User',
        select: 'nickname email photo'
      });
      if (p.sort == 'popular') {
        q.sort([['meta.downloads', 'descending']]);
      } else { //latest
        q.sort([['released', 'descending']]);
      }
      q.limit(limitperpage);

      q.exec(function(err, resp){
        if (err) {
          callback(99997);
        }

        if (resp) {
          errorMessage = 'Success';
          data = {
            "mindmap" : resp
          };
          callback(null, data);
        } else {
          callback(99998);
        }
      });
    }
  }, function (err, results) {
    if (results.data) {
      var total = results.total;
      var data = results.data;
      data.totalCount = total;
      data.serverTime = currTimestamp;
    }

    return callback(err, data);
  });

};

categorySchema.methods.getCategories = function(p,callback) {

  var data = {};
  var err = null;

  console.log('getCategories params',this);

  this.model('Category').findOne({
    'master_user_id': this.master_user_id
  }).exec(function(err, returnObj){

    if(err){
      err = 99997;

    } else if(returnObj){
      data = { 'categories':returnObj.categories, 'aToken':p.aToken } ;

    } else {
      err = 99998;
    }

    return callback(err, data);
  });

};

mindmapSchema.methods.getDetail = function(callback) {

  var data = {};
  var err = null;

  console.log('getDetail params',this);

  this.model('Mindmap').findOne({
    '_id': this._id
  }).populate({
    path: 'author',
    model: 'User',
    select: 'nickname'
  }).populate({
    path: 'reviews.user_id',
    model: 'User',
    select: 'nickname email photo'
  }).exec(function(err, returnObj){

    if(err){
      err = 99997;

    } else if(returnObj){
      data = returnObj ;

    } else {
      err = 99998;
    }

    return callback(err, data);
  });

};

mindmapSchema.methods.putReview = function(p,callback) {

  var data = {};
  var err = null;

  console.log('putReview params',p,this);

  var user_id = p.user_id;
  var score = p.score;
  var title = p.title;
  var comment = p.comment;
  var created = Math.floor(Date.now() / 1000);

  this.model('Mindmap').findOne({
    '_id': this._id
  }).exec(function(err, returnObj){

    if(err){
      err = 99997;

    } else if(returnObj){
      var review = new MindmapReview({
        'user_id': user_id,
        'score': score,
        'comment': comment,
        'title': title,
        'created': created,
      });

      returnObj.review_score = (parseInt(returnObj.review_score) + parseInt(score));
      returnObj.review_count = (parseInt(returnObj.review_count) + 1);
      if (score==1) {
        returnObj.review_count_1 = (parseInt(returnObj.review_count_1) + 1);
      } else if (score==2) {
        returnObj.review_count_2 = (parseInt(returnObj.review_count_2) + 1);
      } else if (score==3) {
        returnObj.review_count_3 = (parseInt(returnObj.review_count_3) + 1);
      } else if (score==4) {
        returnObj.review_count_4 = (parseInt(returnObj.review_count_4) + 1);
      } else if (score==5) {
        returnObj.review_count_5 = (parseInt(returnObj.review_count_5) + 1);
      }

      returnObj.reviews.push(review);
      returnObj.save();

      data = returnObj;

    } else {
      err = 99998;
    }

    return callback(err, data);
  });

};

mindmapSchema.methods.getReviews = function(p,callback) {

  var data = {};
  var err = null;

  var limitperpage = parseInt(p.limit);
  var pagenumber = p.page > 0 ? p.page : 1;
  var startwith = (pagenumber * limitperpage) - limitperpage;

  //console.log('getReviews params',p,this);

  var page = p.page;
  var limit = p.limit;

  var mindmap_id = this._id;

  async.parallel({
    total: function (callback) {
      mongoose.model('Mindmap').findOne({
        '_id': mindmap_id
      }).exec(function (err, results){
        callback(null, results.reviews.length);
      });
    },
    data: function (callback) {
      mongoose.model('Mindmap').findOne({
        '_id': mindmap_id
      },{
        reviews: { $slice: [startwith,limitperpage] }
      }).populate({
        path: 'reviews.user_id',
        model: 'User',
        select: 'nickname email photo'
      }).sort([
        ['reviews.created', 'descending']
      ]).exec(function(err, resp){
        if (err) {
          callback(99997);
        }

        if (resp) {
          errorMessage = 'Success';
          data = {
            'reviews': resp.reviews
          };
          callback(null, data);
        } else {
          callback(99998);
        }
      });
    }
  }, function (err, results) {
    if (results.data) {
      var total = results.total;
      var data = results.data;
      data.totalCount = total;
    }

    return callback(err, data);
  });


};

mindmapSchema.methods.getMyReview = function(p,callback) {

  var data = {};
  var err = null;

  var mindmap_id = this._id;
  var uid = new ObjectID(p.user_id);

  var query = { '_id': mindmap_id, 'reviews.user_id': uid };

  console.log('getMyReview query',query);

  this.model('Mindmap').findOne(query,{
    reviews: true
  }).exec(function(err, returnObj){

    if(err){
      err = 99997;

    } else if(returnObj){

      var reviews ;
      for(i = 0; i < returnObj.reviews.length; i++) {
        if (String(returnObj.reviews[i].user_id) == String(uid)) {
          reviews = returnObj.reviews[i];
        }
      }

      data = reviews;

    } else {
      err = 99998;
    }

    return callback(err, data);
  });

};

mindmapSchema.methods.getMyReviews = function(callback) {

  var data = {};
  var err = null;
  var uid = this._id

  //console.log('getMyReview params',this);

  this.model('Mindmap').find({
    'reviews.user_id': uid
  },{
    reviews: true
  }).exec(function(err, returnObj){

    if(err){
      err = 99997;

    } else if(returnObj){

      var reviewarray = [];
      for(x = 0; x < returnObj.length; x++) {
        for(i = 0; i < returnObj[x].reviews.length; i++) {
          var userid1 = String(uid);
          var userid2 = String(returnObj[x].reviews[i].user_id);
          console.log('compare uid ',returnObj[x].reviews[i], userid1, userid2);
          if (userid1 == userid2) {
            reviewarray.push(returnObj[x].reviews[i]);
          }
        }
      }
      data = reviewarray;

    } else {
      err = 99998;
    }

    return callback(err, data);
  });

};


var Mindmap = mongoose.model('Mindmap', mindmapSchema);
var MindmapReview = mongoose.model('MindmapReview', mindmapReviewSchema);
var Category = mongoose.model('Category', categorySchema);

module.exports = {
  Mindmap: Mindmap,
  MindmapReview: MindmapReview,
  Category: Category,
};
