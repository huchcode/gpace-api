var mongoose = require('mongoose'),
		Schema = mongoose.Schema,
		validator = require('validator');

var appVersionSchema = new Schema({
  channel_id: { type: Schema.Types.ObjectId, ref: 'Channel' },
  os: String,
  version: String,
}, { collection: 'appVersion', versionKey: false });


appVersionSchema.methods.getAppVersion = function(callback) {

  var data = {};

  this.model('AppVersion').findOne({
    'channel_id': this.channel_id,
    'os': this.os
  }).exec(function(err, respObj){

    if(err){
      err = 99997;

    } else if(respObj){
      data = respObj ;

    } else {
      err = 99998;
    }

    return callback(err, data);
  });

};

var AppVersion = mongoose.model('AppVersion', appVersionSchema);

module.exports = {
  AppVersion: AppVersion,
};
