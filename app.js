var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var iap = require('./routes/iap');
var user = require('./routes/user');
var qna = require('./routes/qna');
var comments = require('./routes/comments');
var mindmap = require('./routes/mindmap');

var multer  = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads');
  },
  filename: function (req, file, cb) {
    var originalname = file.originalname.split('.');
    cb(null, file.fieldname + '_' + Date.now() + '.' + originalname[1] );
  }
});
var upload = multer({ storage: storage });

var app = express();

app.post('/image/upload', upload.single('image'), function (req, res, next) {

    //console.log('image/upload req.file : ',req.file);
    var success = {
        errorCode: 0,
        errorMessage: 'Success',
        data: {
            'imageUrl': 'http://' + req.headers.host + req.file.path.replace('public/','/')
        }
    };
    res.status(200);
    res.json(success);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// success json message
app.use(function (req, res, next) {
  res.success = function (json) {
    var success = {
        errorCode: 0,
        errorMessage: 'Success'
    };
    success.data = json;
    res.json(success);
  };
  next();
});

app.use('/', routes);
app.use('/', iap);
app.use('/', user);
app.use('/', qna);
app.use('/', comments);
app.use('/', mindmap);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(errorCode, req, res, next) {
        // console.log(errorCode);
        // err as json
        var error = {
            errorCode: errorCode,
            errorMessage: errorcodes['E' + errorCode]
        };
        res.status(200);
        res.json(error);
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
