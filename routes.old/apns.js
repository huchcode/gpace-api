var express = require('express');
var join = require('path').join;
var router = express.Router();

var apnagent = require('apnagent');
var agent = new apnagent.Agent();

var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    //BSON = require('mongodb').pure().BSON,
    assert = require('assert'),
    validator = require('validator'),
    schedule = require('node-schedule');

var mongoose = require('mongoose');
var UserModel = require('../models/users');

// Make pem file.
// openssl pkcs12 -clcerts -nokeys -out cert.pem -in cert.p12
// openssl pkcs12 -nocerts -out key.pem -in key.p12
// openssl rsa -in key.pem -out key.pem

agent 
	.set('cert file', join(__dirname, '../cert/gfg-cert.pem'))
	.set('key file', join(__dirname, '../cert/gfg-key.pem'))
	.enable('sandbox');

agent.connect(function (err) {
	if (err) throw err;
});

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'APNS' });
});

router.post('/sendmessage', function(req, res) {

	var user_id = req.body.user_id;
	var alert = req.body.alert;
	var badge = req.body.badge;
	var ntype = req.body.ntype;
	var token = "";

	if (user_id == "" || alert == "") {
		res.setHeader('Content-Type', 'application/json');
	    var json = JSON.stringify({
	      "errorCode":90000,
	      "errorMessage":"Missing parameters",
	    });
	    res.send(json);
	}

	// get token from user_id
	var UserModel = mongoose.model('User');
	var usr = new UserModel({ '_id': user_id });
	usr.viewprofile(function(u){

		try {
			token = u.data.user.device.deviceToken;

			// send push
			if (token != ""){
				var err = apnsSend(token, alert, badge, ntype);

				if(err){
					console.log(err);
					res.setHeader('Content-Type', 'application/json');
				    var json = JSON.stringify({
				      "errorCode":90000,
				      "errorMessage":err,
				    });
				    res.send(json);
				}
			}
		} catch(e){
			console.log(e);
		}

		res.setHeader('Content-Type', 'application/json');
	    var json = JSON.stringify({
	      "errorCode": 0,
	      "errorMessage": "Send push message"
	    });
	    res.send(json);
	});	
});

router.post('/sendpush', function(req, res) {

	var token = req.body.token;
	var alert = req.body.alert;
	var badge = req.body.badge;
	var ntype = req.body.ntype;

	// for test
	// alert = "alert message";
	// token = "e4494787d55667c252f67c8ce5f4f68c9fecfbacf8ee213881226cb2b637dfcf";
	// badge = 1;

	if (token == "" || alert == "") {
		res.setHeader('Content-Type', 'application/json');
	    var json = JSON.stringify({
	      "errorCode":90000,
	      "errorMessage":"Missing parameters",
	    });
	    res.send(json);
	}

	// send push
	var err = apnsSend(token, alert, badge, ntype);

	if(err){
		console.log(err);
		res.setHeader('Content-Type', 'application/json');
	    var json = JSON.stringify({
	      "errorCode":90000,
	      "errorMessage":err,
	    });
	    res.send(json);
	}

	res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify({
      "errorCode": 0,
      "errorMessage": "Send push message"
    });
    res.send(json);
});

var apnsSend = function (token, alert, badge, ntype){

	agent.createMessage()
	.device(token)
	.alert(alert)
	.badge(badge)
	.sound('bingbong.aiff')
	.set({ntype:ntype})
	.send(function (err) {
		return err;
	});
};

module.exports = router;
