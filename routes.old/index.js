var express = require('express');
var router = express.Router();

var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    assert = require('assert'),
    validator = require('validator'),
    schedule = require('node-schedule'),
    fbsdk = require('facebook-sdk'),
    os = require('os');

var md5 = require('MD5');

var facebook = new fbsdk.Facebook({ appId  : '761587267211259', secret : '1ce6d8ffe51dd825158258ad54835bba' });

var mongoose = require('mongoose');

var networkInterfacesObj = os.networkInterfaces();

if(networkInterfacesObj.enp0s25) {
  var localIPAddress = networkInterfacesObj.enp0s25[0].address;
} else if(networkInterfacesObj.eth0) {
  var localIPAddress = networkInterfacesObj.eth0[0].address;
}

if (localIPAddress == '192.168.0.112') {
  var devserver = true;
} else {
  var devserver = false;
}

if (devserver) {
  var mongooseConnectionString = 'mongodb://huchNormal:kitiwit7@192.168.0.112:27017/api';
} else {
  var mongooseConnectionString = 'mongodb://gpac:wlvorakzptvmffpdltm2016@115.68.185.122:27017/gpace';
}
var options = { server: { socketOptions: { keepAlive: 1 } } };
var mongoconn = mongoose.connect(mongooseConnectionString,options);
//console.log(mongoconn);

require('../models/errors');

var UserModel = require('../models/users');
var User = UserModel.User;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  // yay!
});

var iap = require('in-app-purchase');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Node JS API' });
});

router.get('/testform', function(req, res) {
  res.render('testform', { title: 'Test Form' });
});


require('../models/comments');
var Comments = mongoose.model('Comments');

router.post('/addcomment', function(req, res) {

  var user_id = req.body.user_id;
  var user_name = req.body.user_name;
  var user_photo = req.body.user_photo;
  var content_id = req.body.content_id;
  var comment = req.body.comment;
  var link = req.body.link;
  var timestamp = req.body.timestamp;

  if (timestamp == "" || validator.isNull(timestamp)) {
    var timestamp = Math.floor(Date.now() / 1000);
  }

  if (validator.isNull(link)) {
    var link = "";
  }

  if ( content_id == "" || validator.isNull(content_id) ) {

    // If it failed, return error
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify({
      "errorCode":11002,
      "errorMessage":errorcodes.E11002,
      "data":{}
    });
    res.send(json);

  } else if ( user_id == "" || validator.isNull(user_id) ) {

    // If it failed, return error
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify({
      "errorCode":11001,
      "errorMessage":errorcodes.E11001,
      "data":{}
    });
    res.send(json);

  } else if ( comment == "" || validator.isNull(comment) ) {

    // If it failed, return error
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify({
      "errorCode":11003,
      "errorMessage":errorcodes.E11003,
      "data":{}
    });
    res.send(json);

  } else {

    var Comments = mongoose.model('Comments');
    var datas = {
      "user_id" : user_id,
      "user_name" : user_name,
      "user_photo" : user_photo,
      "content_id" : content_id,
      "comment" : comment,
      "link" : link,
      "timestamp" : timestamp
    };
    console.log(datas);
    var Comments = new Comments(datas);

    // Submit to the DB
    Comments.save(function (err) {
        if (err) {
          res.setHeader('Content-Type', 'application/json');
          var json = JSON.stringify({
            "errorCode" :99996,
            "errorMessage":errorcodes.E99996+'. '+err,
            "data":{}
          });
          res.send(json);
        }
        else {
          res.setHeader('Content-Type', 'application/json');
          var json = JSON.stringify({
            "errorCode":0,
            "errorMessage":"Success",
            "data":{"Comments" : Comments}
          });
          res.send(json);
        }
    });


  }

});

router.post('/getcomments', function(req, res) {

  var user_id = req.body.user_id;
  var content_id = req.body.content_id;

  var limit = req.body.limit;
  var page = req.body.page;
  var sort = req.body.sort;
  var order = req.body.order;

  if (limit == "" || validator.isNull(limit)){
    limit = 10;
  }
  if (page == "" || validator.isNull(page)){
    page = 1;
  }
  if (sort == "" || validator.isNull(sort)){
    sort = 'timestamp';
  }
  if (order == "" || validator.isNull(order)){
    order = 'descending';
  }

  var comments = new Comments({'user_id':user_id,'content_id':content_id,'limit':limit,'page':page,'sort':sort,'order':order});
  comments.getcomments(function(u){
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify(u);
    res.send(json);
  });

});

router.post('/deletecomment', function(req, res) {

  var comment_id = req.body.comment_id;

  var comments = new Comments({'_id':comment_id});
  comments.deletecomment(function(u){
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify(u);
    res.send(json);
  });

});


require('../models/announcement');
var Announcement = mongoose.model('Announcement');
var AnnouncementRead = mongoose.model('AnnouncementRead');

router.post('/getannouncementscount', function(req, res) {

  var user_id = req.body.user_id;
  var master_user_id = req.body.master_user_id;

  var announcement = new Announcement({'master_user_id':master_user_id,'user_id':user_id});
  announcement.getannouncementcount(function(u){
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify(u);
    res.send(json);
  });

});

router.post('/getannouncements', function(req, res) {

  var user_id = req.body.user_id;
  var master_user_id = req.body.master_user_id;

  var limit = req.body.limit;
  var page = req.body.page;
  var sort = req.body.sort;
  var order = req.body.order;

  if ( master_user_id == "" || validator.isNull(master_user_id) ) {

    // If it failed, return error
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify({
      "errorCode":11005,
      "errorMessage":errorcodes.E11005,
      "data":{}
    });
    res.send(json);

  } else if ( user_id == "" || validator.isNull(user_id) ) {

    // If it failed, return error
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify({
      "errorCode":11001,
      "errorMessage":errorcodes.E11001,
      "data":{}
    });
    res.send(json);

  }
  if (limit == "" || validator.isNull(limit)){
    limit = 10;
  }
  if (page == "" || validator.isNull(page)){
    page = 1;
  }
  if (sort == "" || validator.isNull(sort)){
    sort = 'start_timestamp';
  }
  if (order == "" || validator.isNull(order)){
    order = 'descending';
  }

  var announcementRead = new AnnouncementRead({'user_id':user_id});
  announcementRead.updateannouncementread(function(u){});

  var announcement = new Announcement({'master_user_id':master_user_id,'limit':limit,'page':page,'sort':sort,'order':order});
  announcement.getannouncements(function(u){
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify(u);
    res.send(json);
  });

});

router.post('/getannouncement', function(req, res) {

  var announcement_id = req.body.announcement_id;

  if ( announcement_id == "" || validator.isNull(announcement_id) ) {

    // If it failed, return error
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify({
      "errorCode":11006,
      "errorMessage":errorcodes.E11006,
      "data":{}
    });
    res.send(json);

  }

  var announcement = new Announcement({'_id':announcement_id});
  announcement.getannouncement(function(u){
    res.setHeader('Content-Type', 'application/json');
    var json = JSON.stringify(u);
    res.send(json);
  });

});


module.exports = router;
