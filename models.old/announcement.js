var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator');

var announcementSchema = new Schema({
  master_user_id: Number,
  title: String,
  description: String,
  start_timestamp: Number,
  created: Number,

  limit: Number,
  page: Number,
  sort: String,
  order: String,

  user_id: Number

}, { collection: 'announcement', versionKey: false });

var announcementReadSchema = new Schema({
  user_id: Number,
  last_read_timestamp: Number

}, { collection: 'announcementRead', versionKey: false });

announcementSchema.methods.getannouncements = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  limitperpage = this.limit;
  pagenumber = this.page;
  sortwhat = this.sort;
  sortorder = this.order;

  var startwith = (pagenumber * limitperpage) - limitperpage;

  var currTimeStamp = Math.floor(Date.now() / 1000);

  console.log('getannouncements params',this);

  var q;
  q = this.model('Announcement').find({
    'master_user_id': this.master_user_id,
    'start_timestamp':{ $lt: currTimeStamp }
  },{'description':false});

  q.skip(startwith).limit(limitperpage).sort([[ sortwhat, sortorder]]);
  q.exec(function(err, resp){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(resp){
      errorMessage = "Success";
      data = { "Announcement" : resp };
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });

};

announcementSchema.methods.getannouncement = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  console.log('getannouncement params',this);

  var q;
  q = this.model('Announcement').find({'_id': this._id});
  q.exec(function(err, announcement){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(announcement){
      errorMessage = "Success";
      data = { "announcement" : announcement };
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });

};

announcementSchema.methods.getannouncementcount = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  console.log('getannouncementscount params',this);

  getannouncementlasttimeread(this, function(last_read_timestamp,master_user_id){

    console.log('getannouncementlasttimeread resp',last_read_timestamp,master_user_id);
    getannouncementremainingcount({
      'last_read_timestamp':last_read_timestamp,
      'master_user_id':master_user_id,
    }, function(announcementCount){

      console.log('announcementCount',announcementCount);
      data = { "count": announcementCount }
      r = { "errorCode": errorCode, "errorMessage": "Success", "data": data };
      return callback(r);

    });
  });

};

announcementReadSchema.methods.getannouncementread = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  //console.log('getannouncementread params',this);

  var q;
  q = this.model('AnnouncementRead').find({
    'user_id': this.user_id
  });
  q.exec(function(err, announcementRead){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(announcementRead){
      errorMessage = "Success";
      data = { "AnnouncementRead" : announcementRead };
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });

};

announcementReadSchema.methods.updateannouncementread = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  var modified = Math.floor(Date.now() / 1000);

  var query = {'user_id':this.user_id};
  var update = {'last_read_timestamp':modified};
  var option = {upsert:true,safe:false};

  console.log('upsert updateannouncementread',update,query)

  var q;
  q = this.model('AnnouncementRead').update(query,update,option);
  q.exec(function(err, AnnouncementRead){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(AnnouncementRead){
      errorMessage = "Success";
      data = { "AnnouncementRead" : {'user_id':this.user_id,'last_read_timestamp':modified} };
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });

};

function getannouncementlasttimeread(params,cb) {

  var query = mongoose.model('AnnouncementRead').findOne({'user_id': params.user_id });
  query.select('last_read_timestamp');
  query.exec(function(err, announceread){
    if (announceread) {
      var response = announceread.last_read_timestamp;
    } else {
      var response = 1420070400;
    }
    return cb(response,params.master_user_id);
  });
}

function getannouncementremainingcount(params,cb) {
  var last_read_timestamp = params.last_read_timestamp;
  var master_user_id = params.master_user_id;

  var currTimeStamp = Math.floor(Date.now() / 1000);
  var filter = {
    'master_user_id': master_user_id,
    'start_timestamp':{$gt:last_read_timestamp,$lt:currTimeStamp}
  };

  var query = mongoose.model('Announcement').find(filter);
  query.count();
  query.exec(function(err, count){
    if ( validator.isNull(count) ) {
      count = 0;
    }
    console.log('getannouncementremainingcount params',filter)
    return cb(count);
  });
}



mongoose.model('Announcement', announcementSchema);
mongoose.model('AnnouncementRead', announcementReadSchema);
