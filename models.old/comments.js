var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator');

var commentsSchema = new Schema({
  user_id: Number,
  user_name: String,
  user_photo: String,
  content_id: Number,
  comment: String,
  link: String,
  timestamp: String,

  limit: Number,
  page: Number,
  sort: String,
  order: String

}, { collection: 'comments', versionKey: false });

commentsSchema.methods.getcomments = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  limitperpage = this.limit;
  pagenumber = this.page;
  sortwhat = this.sort;
  sortorder = this.order;

  var startwith = (pagenumber * limitperpage) - limitperpage;

  console.log('getcomments params',this);

  var q;
  if (validator.isNull(this.user_id)) {
    q = this.model('Comments').find({'content_id': this.content_id});
  } else if (validator.isNull(this.content_id)) {
    q = this.model('Comments').find({'user_id': this.user_id});
  } else {
    q = this.model('Comments').find({'user_id': this.user_id,'content_id': this.content_id});
  }


  q.skip(startwith).limit(limitperpage).sort([[ sortwhat, sortorder]]);
  q.exec(function(err, comments){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(comments){
      errorMessage = "Success";
      data = { "Comments" : comments };
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });

};

commentsSchema.methods.deletecomment = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this._id)){
    errorCode = 11004;
    errorMessage = errorcodes.E11004;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q;
    q = this.model('Comments').findByIdAndRemove({'_id':this._id});
    q.exec(function(err, comment){
      if(err){
        errorCode = 99995;
        errorMessage = errorcodes.E99995+'. '+err;
      }

      if(comment){
        errorMessage = "Success";
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

mongoose.model('Comments', commentsSchema);
