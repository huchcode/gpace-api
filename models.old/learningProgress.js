var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator');

var learningProgressSchema = new Schema({
  doneDate: Number,
  lecture_id: String,
  type: Number,
  user_id: String
}, { collection: 'learningProgress', versionKey: false });

learningProgressSchema.methods.checkifexists = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};
  
  var q;
  q = this.model('LearningProgress').find({'user_id': this.user_id, 'lecture_id': this.lecture_id, 'type': this.type});

  q.exec(function(err, lprog){

    if(Object.keys(lprog).length == 0){
      r = 'not exists';
    } else {
      r = 'already existing';
    }
    return callback(r);
  });
  
};

learningProgressSchema.methods.getlearningprogress = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};


  if (validator.isNull(this.user_id)){
    r = {
      "errorCode": E11003,
      "errorMessage": errorcodes.E11001,
      "data": data
    };
    return callback(r);
  } else if (validator.isNull(this.doneDate)){
    r = {
      "errorCode": 11025,
      "errorMessage": errorcodes.E11025,
      "data": data
    };
    return callback(r);
  } else {

    var q;
    q = this.model('LearningProgress').find({'user_id': this.user_id, 'doneDate': { '$gt': this.doneDate }});
  
    q.exec(function(err, lprog){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }
  
      if(lprog){
        errorMessage = "Success";
        data = lprog;
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }
  
      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

mongoose.model('LearningProgress', learningProgressSchema);