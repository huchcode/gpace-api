var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    validator = require('validator'),
    md5 = require('MD5'),
    fbsdk = require('facebook-sdk');

var facebook = new fbsdk.Facebook({ appId  : '761587267211259', secret : '1ce6d8ffe51dd825158258ad54835bba' });

var userSchema = new Schema({
  accuPnt: Number,
  appSetting: {
    isIncLower: Number,
    lang: String,
    publishers_id: String,
    studyGrade: Number,
    timezone: Number
  },
  currPnt: Number,
  device: {
    deviceToken: String,
    modelCode: String,
    os: String
  },
  email: String,
  loginID: String,
  garbageUserID: String,
  evernote: {
    userID: String,
    displayName: String,
    notebookGUID: String
  },
  facebook: {
    ageRange: String,
    birthDay: String,
    firstName: String,
    gender: String,
    id: String,
    imgUrl: String,
    lastName: String,
    locale: String,
    modified: String,
    name: String,
    timezone: Number,
    userLocation: String
  },
  joinStatus: {
    blocked: Number,
    transfered: Number,
    left: Number,
    created: Number,
    requested: Number,
    requestLeave: Number,
    status: Number
  },
  joinWay: { type: Number, default: 1 },
  name: String,
  password: String,
  photoURL: String,
  schoolName: String,
  selfIntro: String,
  twitter: {
    id: String,
    imgURL: String,
    locale: String,
    name: String,
    timezone: Number
  },
  userLevelDefine_id: String,
  vcode: String,
  country: String,
  countryCode: String,
  city: String,
  location: String
}, { collection: 'user', versionKey: false });


var currTimestamp = new Date().getTime();

var pointSchema = new Schema({
  audio_id: String,
  caseName: Schema.Types.Mixed,
  coefficient: Number,
  comment: String,
  fromU_id: String,
  fromUserName: String,
  lecture_id: String,
  movie_id: String,
  pnt: Number,
  pointCase_id: String,
  structure_id: String,
  user_id: String,
  userName: String,
  obtain_date: { type: Number, default: currTimestamp }
}, { collection: 'point', versionKey: false });

var userLevelDefineSchema = new Schema({
  _id: String,
  levelName: Schema.Types.Mixed,
  minPnt: Number,
  maxPnt: Number
}, { collection: 'userLevelDefine', versionKey: false });

var pointCaseSchema = new Schema({
  _id: String,
  caseName: Schema.Types.Mixed,
  coefficient: Number,
  pnt: Number
}, { collection: 'pointCase', versionKey: false });

var currentRankingSchema = new Schema({
  rank: Number,
  user_id: String,
  email: String,
  photoURL: String,
  name: String,
  userLevelDefine_id: String,
  accuPnt: Number,
  country: String,
  facebook_id: String,

  filter: String,
  limit: Number,
  page: Number,
  sort: String,
  order: String
}, { collection: 'currentRanking', versionKey: false });

var weeklyRankingSchema = new Schema({
  rank: Number,
  user_id: String,
  email: String,
  photoURL: String,
  name: String,
  userLevelDefine_id: String,
  accuPnt: Number,
  country: String,
  facebook_id: String,

  filter: String,
  limit: Number,
  page: Number,
  sort: String,
  order: String
}, { collection: 'weeklyRanking', versionKey: false });

var appInfoSchema = new Schema({
  version: String,
  date: Number,
  message: String
}, { collection: 'appInfo', versionKey: false });

var salesSchema = new Schema({
  salesDate: String,
  salesProduct: Schema.Types.Mixed,
  buyer: Schema.Types.Mixed,
  receipt: Schema.Types.Mixed,
  status: Number,
  user_id: { type: Schema.Types.ObjectId, ref: 'User' },
  product_id: String
  
}, { collection: 'sales', versionKey: false });

var salesLogSchema = new Schema({
  sales_id: { type: Schema.Types.ObjectId, ref: 'Sales' },
  transDate: Number,
  transStatus: Number,
  user_id: { type: Schema.Types.ObjectId, ref: 'User' }
}, { collection: 'salesLog', versionKey: false });

userSchema.methods.login = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if(validator.isNull(this.joinWay)) {
    this.joinWay = 1;
  }

  /*if (validator.isNull(this.email)){
    errorCode = 10001;
    errorMessage = errorcodes.E10001;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else if (!validator.isEmail(this.email)){
    errorCode = 10002;
    errorMessage = errorcodes.E10002;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else*/ 
  
  if(1 == this.joinWay && validator.isNull(this.password)) {
    errorCode = 10004;
    errorMessage = errorcodes.E10004;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q;
    if( 1 == this.joinWay ){
      q = this.model('User').findOne({'loginID': this.loginID, 'password': md5(this.password)});
    } else if( 2 == this.joinWay ){
      q = this.model('User').findOne({'email': this.email});
    }
    
    /* else {
      q = this.model('User').findOne({'email': this.loginID});
    }*/

    q.exec(function(err, user){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }

      if(user){
        delete user.password;
        errorMessage = "Success";
        data = { "user" : user };
      } else {
        errorCode = 10007;
        errorMessage = errorcodes.E10007;
      }

      r = {
        "errorCode": errorCode,
        "errorMessage": errorMessage,
        "data": data
      };

      return callback(r);
    });
  }
};

userSchema.methods.changepassword = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this._id)){
    errorCode = 11001;
    errorMessage = errorcodes.E11001;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else if(validator.isNull(this.password)) {
    errorCode = 10004;
    errorMessage = errorcodes.E10004;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q;
    var query = {'_id':this._id};
    var update = {'password':this.password};
    var option = {};

    q = this.model('User').update(query,update,option);

    q.exec(function(err, user){
      if(err){
        errorCode = 99995;
        errorMessage = errorcodes.E99995+'. '+err;
      }

      if(user){
        errorMessage = "Success";
        data = {};
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

userSchema.methods.registerupdateuser = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this._id)){
    errorCode = 11001;
    errorMessage = errorcodes.E11001;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else if(validator.isNull(this.password)) {
    errorCode = 10004;
    errorMessage = errorcodes.E10004;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q;
    var query = {'_id':this._id};
    var update = {'password':this.password};
    var option = {};

    q = this.model('User').update(query,update,option);

    q.exec(function(err, user){
      if(err){
        errorCode = 99995;
        errorMessage = errorcodes.E99995+'. '+err;
      }

      if(user){
        errorMessage = "Success";
        data = {};
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

userSchema.methods.resetpassword = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this.email)){
    errorCode = 10001;
    errorMessage = errorcodes.E10001;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else if(validator.isNull(this.password)) {
    errorCode = 10004;
    errorMessage = errorcodes.E10004;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q;
    var query = {'email':this.email};
    var update = {'password':this.password};
    var option = {};

    q = this.model('User').update(query,update,option);

    q.exec(function(err, user){
      if(err){
        errorCode = 99995;
        errorMessage = errorcodes.E99995+'. '+err;
      }

      if(user){
        errorMessage = "Success";
        data = {};
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

userSchema.methods.viewprofile = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this._id)){

    errorCode = 11001;
    errorMessage = errorcodes.E11001;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);

  } else {
    var q;
    q = this.model('User').findOne({'_id': this._id});

    q.exec(function(err, user){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }

      if(user){
        delete user.password;

        getuserLevelDefine(user.userLevelDefine_id, function(userLevelDefine){

          getuserpersonalranking(user._id, function(userRank){

            getuserpersonalweeklyranking(user._id, function(userweeklyRank){

              errorMessage = "Success";
              if (userLevelDefine && userRank && userweeklyRank) {
                data = {  "user" : user , "userLevelDefine" : { "levelName" : userLevelDefine.levelName }, "currentRanking" : { "rank" : userRank.rank } , "weeklyRanking" : { "rank" : userweeklyRank.rank } };
              } else if (!userLevelDefine && !userRank && !userweeklyRank) {
                data = {  "user" : user , "userLevelDefine" : { "levelName" : "" }, "currentRanking" : { "rank" : "" } , "weeklyRanking" : { "rank" : "" } };
              } else if (userLevelDefine && userRank && !userweeklyRank) {
                data = {  "user" : user , "userLevelDefine" : { "levelName" : userLevelDefine.levelName }, "currentRanking" : { "rank" : userRank.rank } , "weeklyRanking" : { "rank" : "" } };
              } else if (userLevelDefine && !userRank && userweeklyRank) {
                data = {  "user" : user , "userLevelDefine" : { "levelName" : userLevelDefine.levelName }, "currentRanking" : { "rank" : "" } , "weeklyRanking" : { "rank" : userweeklyRank.rank } };
              } else if (!userLevelDefine && userRank && userweeklyRank) {
                data = {  "user" : user , "userLevelDefine" : { "levelName" : "" }, "currentRanking" : { "rank" : userRank.rank } , "weeklyRanking" : { "rank" : userweeklyRank.rank } };
              } else if (userLevelDefine && !userRank && !userweeklyRank) {
                data = {  "user" : user , "userLevelDefine" : { "levelName" : userLevelDefine.levelName }, "currentRanking" : { "rank" : "" } , "weeklyRanking" : { "rank" : "" } };
              } else if (!userLevelDefine && !userRank && userweeklyRank) {
                data = {  "user" : user , "userLevelDefine" : { "levelName" : "" }, "currentRanking" : { "rank" : "" } , "weeklyRanking" : { "rank" : userweeklyRank.rank } };
              } else if (!userLevelDefine && userRank && !userweeklyRank) {
                data = {  "user" : user , "userLevelDefine" : { "levelName" : "" }, "currentRanking" : { "rank" : userRank.rank } , "weeklyRanking" : { "rank" : "" } };
              }
              r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
              return callback(r);
            });
          });
        });

      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
        r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
        return callback(r);
      }

    });
  }
};

userSchema.methods.getfacebookuserinfo = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this.facebook.id)){

    errorCode = 11002;
    errorMessage = errorcodes.E11002;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);

  } else {
    var q;
    q = this.model('User').findOne({'facebook.id': this.facebook.id});

    q.exec(function(err, user){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }

      if(user){
        delete user.password;
        errorMessage = "Success";
        data = { "user" : user };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};


salesSchema.methods.cancelPurchase = function(callback) {

  var errorCode = 0;
  var errorMessage = {};
  var data = {};
  var r = {};
    
  
  var s;
  s = this.model('Sales').findOne({'user_id': this.user_id,'product_id': this.product_id});

  s.exec(function(err, sales){
  
    if(err){
      console.log(err);
    }

    if(sales){
       
      var q;
      var query = {'user_id':sales.user_id,'product_id':sales.product_id};
      var update = {'status':3};
      var option = {};
    
      q = mongoose.model('Sales').update(query,update,option);
    
      q.exec(function(err, ret){
        if(err){
          errorCode = 99995;
          errorMessage = errorcodes.E99995+'. '+err;
        }
    
        if(ret){
    
          var SalesLog = mongoose.model('SalesLog');
          var logDatas = {
            "sales_id" : sales._id,
            "transDate" : currTimestamp,
            "transStatus" : 3,
            "user_id" : sales.user_id
          };
          var salesLog = new SalesLog(logDatas);
          salesLog.save();
    
          errorMessage = "Success";
          data = sales;
          
        } else {
          errorCode = 99998;
          errorMessage = errorcodes.E99998;
        }
    
        r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
        return callback(r);
      });
      
    }
  });

};

salesSchema.methods.checkDuplicateReceipt = function(callback) {

  var errorCode = 0;
  var errorMessage = {};
  var data = {};
  var r = {};

  var q;
  q = this.model('Sales').findOne({'user_id': this.user_id,'product_id': this.product_id,'status': 1});

  q.exec(function(err, sales){
    if(err){
      console.log(err);
    }

    if(sales){
      errorCode = 50004;
      errorMessage = errorcodes.E50004;
    } else {
      errorMessage = "No Duplicate";
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });
};

userSchema.methods.getUserInfo = function(callback) {

  var errorCode = 0;
  var errorMessage = {};
  var data = {};
  var r = {};

  var q;
  q = this.model('User').findOne({'_id': this._id});

  q.exec(function(err, user){
    if(err){
      console.log(err);
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": user };
    return callback(r);
  });
};

userSchema.methods.checkemail = function(callback) {

  var errorCode = 0;
  var errorMessage = {};
  var data = {};
  var r = {};

  var q;
  q = this.model('User').findOne({'email': this.email});

  q.exec(function(err, user){
    if(err){
      //console.log(err);
    }

    if(user){
      errorCode = 10003;
      errorMessage = errorcodes.E10003;
    } else {
      errorMessage = "Success";
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });
};


userSchema.methods.checkloginid= function(callback) {

  var errorCode = 0;
  var errorMessage = {};
  var data = {};
  var r = {};

  var q;
  q = this.model('User').findOne({'loginID': this.loginID});

  q.exec(function(err, user){
    if(err){
      //console.log(err);
    }

    if(user){
      errorCode = 10010;
      errorMessage = errorcodes.E10010;
    } else {
      errorMessage = "Success";
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });
};

userSchema.methods.checkpassword = function(callback) {

  var errorCode = 0;
  var errorMessage = {};
  var data = {};
  var r = {};

  var q;
  q = this.model('User').findOne({'_id': this._id, 'password': this.password});

  q.exec(function(err, user){
    if(err){
      console.log('checkpassword',err);
    }

    if(user){
      errorCode = 10008;
      errorMessage = errorcodes.E10008;
    } else {
      errorMessage = "Success";
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": user };
    return callback(r);
  });
};

userSchema.methods.register = function(callback){
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  var currTimestamp = new Date().getTime();

  if (validator.isNull(this.joinWay)) {
    this.joinWay = 1;
  }
  if (validator.isNull(accuPnt)) {
    var accuPnt = 0;
  }
  if (validator.isNull(currPnt)) {
    this.currPnt = 0;
  }

  if(validator.isNull(this.email)){
    errorCode = 10001;
    errorMessage = errorcodes.E10001;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else if( this.joinWay==1 && validator.isNull(this.password)) {
    errorCode = 10004;
    errorMessage = errorcodes.E10004;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  }
};

userSchema.methods.getfriends = function(callback) {

  var errorCode = 0;
  var errorMessage = {};
  var data = {};
  var r = {};

  var q;
  q = this.model('User').findOne({'_id': this._id});

  q.exec(function(err, user){
    if(err){
      console.log(err);
    }

    if(user){

      getfacebookfriends(user.facebook.id, function(fbfriends){

        //console.log(fbfriends);
        var query = mongoose.model('User').find();

        query.where('facebook.id').in(fbfriends);
        query.where('facebook.id').ne(user.facebook.id);
        query.exec(function(err, users){
          r = { "errorCode": errorCode, "errorMessage": "Success", "data": { "user": users } };
          return callback(r);
        });

      });

    } else {
      errorCode = 12001;
      errorMessage = errorcodes.E12001;

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    }
  });

};

appInfoSchema.methods.getlatestappversion = function(callback) {

  var errorCode = 0;
  var errorMessage = {};
  var data = {};
  var r = {};
  
  getlatestappversion(function(appLatestVer){

    var appReturn = {
      "version" : appLatestVer.version,
      "updated" : appLatestVer.updated,
      "message" : appLatestVer.message
    };

    console.log(appReturn);
    r = { "errorCode": errorCode, "errorMessage": "Success", "data": { "appInfo": appReturn} };
    return callback(r);

  });

};

appInfoSchema.methods.getappversion = function(callback) {

  var errorCode = 0;
  var errorMessage = {};
  var data = {};
  var r = {};

  console.log(this);

  if (validator.isNull(this.version)) {

    errorCode = 11022;
    errorMessage = errorcodes.E11022;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);

  }

  var q;
  q = this.model('AppInfo').findOne({'version': this.version});

  q.exec(function(err, appVer){
    if(err){
      console.log(err);
    }

    if(appVer){


      getlatestappversion(function(appLatestVer){

        var appReturn = {
          "version" : appVer.version,
          "updated" : appVer.updated,
          "message" : appVer.message,
          "latest_version" : appLatestVer.version
        };

        console.log(appReturn);
        r = { "errorCode": errorCode, "errorMessage": "Success", "data": { "appInfo": appReturn} };
        return callback(r);

      });

    } else {
      errorCode = 12022;
      errorMessage = errorcodes.E12022;

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    }
  });

};

pointSchema.methods.getpointhistory = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};


  if (validator.isNull(this.user_id)) {

    errorCode = 11001;
    errorMessage = errorcodes.E11001;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": {} };
    return callback(r);

  }

  var q;
  q = this.model('Point').find({'user_id': this.user_id});

  q.exec(function(err, userpoints){
    if(err){
      console.log(err);
    }

    if(userpoints){

      getPointCaseCollection(function(pontCase){
        data = { "pointCase": pontCase, "point": userpoints };
        r = { "errorCode": errorCode, "errorMessage": "Success", "data": data };
        return callback(r);
      });


    } else {
      errorCode = 12001;
      errorMessage = errorcodes.E12001;

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    }
  });

};

currentRankingSchema.methods.getrankingbeforeandafter = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  var limitcount = 6;


  if (validator.isNull(this.user_id)) {

    errorCode = 11001;
    errorMessage = errorcodes.E11001;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": {} };
    return callback(r);

  }

  q = this.model('CurrentRanking').findOne({'user_id': this.user_id});
  q.exec(function(err, userrankinfo){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    }

    if(userrankinfo){
      getuserrankneighbor(userrankinfo.user_id,userrankinfo.rank,limitcount,'rank','descending', function(gtrank){
        getuserrankneighbor(userrankinfo.user_id,userrankinfo.rank,limitcount,'rank','ascending', function(ltrank){
          data = { "lowerRanks": gtrank ,"upperRanks": ltrank };
          r = { "errorCode": errorCode, "errorMessage": "Success", "data": data };
          return callback(r);
        });
      });

    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    }
  });


};

currentRankingSchema.methods.getcurrentranking = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  limitperpage = this.limit;
  pagenumber = this.page;
  sortwhat = this.sort;
  sortorder = this.order;

  var startwith = (pagenumber * limitperpage) - limitperpage;

  //console.log(this);

  var q;
  if (this.filter=='friends') {

    getuserinfo(this.user_id, function(data){
      if (validator.isNull(data.user)) {

        errorCode = 12001;
        errorMessage = errorcodes.E12001;
        r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": {} };
        return callback(r);

      } else {

        if (validator.isNull(data.user.facebook)) {

          errorCode = 50001;
          errorMessage = errorcodes.E50001;
          r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": {} };
          return callback(r);

        } else {

          getfacebookfriends(data.user.facebook.id, function(fbfriends){

            console.log(fbfriends);
            var query = mongoose.model('CurrentRanking').find();

            query.where('facebook_id').in(fbfriends);
            query.sort('rank');
            query.skip(startwith).limit(limitperpage).sort([[ sortwhat, sortorder]]);
            query.exec(function(err, userranks){
              r = { "errorCode": errorCode, "errorMessage": "Success", "data": { "currentRanking": userranks } };
              return callback(r);
            });

          });
        }
      }
    });

  } else if (this.filter=='country') {

    getuserinfo(this.user_id, function(data){
      if (validator.isNull(data.user)) {

        errorCode = 12001;
        errorMessage = errorcodes.E12001;
        r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": {} };
        return callback(r);

      } else {

        if (validator.isNull(data.user.facebook)) {

          errorCode = 50001;
          errorMessage = errorcodes.E50001;
          r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": {} };
          return callback(r);

        } else {

          var query = mongoose.model('CurrentRanking').find();

          query.where('country').equals(data.user.country);
          query.sort('rank');
          query.skip(startwith).limit(limitperpage).sort([[ sortwhat, sortorder]]);
          query.exec(function(err, userranks){
            r = { "errorCode": errorCode, "errorMessage": "Success", "data": { "currentRanking": userranks } };
            return callback(r);
          });
        }
      }
    });


  } else {
    q = this.model('CurrentRanking').find();

    q.skip(startwith).limit(limitperpage).sort([[ sortwhat, sortorder]]);
    q.exec(function(err, ranks){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }

      if(ranks){
        errorMessage = "Success";
        data = { "currentRanking" : ranks };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }

};

weeklyRankingSchema.methods.getweeklyranking = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  limitperpage = this.limit;
  pagenumber = this.page;
  sortwhat = this.sort;
  sortorder = this.order;

  var startwith = (pagenumber * limitperpage) - limitperpage;

  //console.log(this);
  var q;
  if (this.filter=='friends') {

    getuserinfo(this.user_id, function(data){
      if (validator.isNull(data.user)) {

        errorCode = 12001;
        errorMessage = errorcodes.E12001;
        r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": {} };
        return callback(r);

      } else {

        if (validator.isNull(data.user.facebook)) {

          errorCode = 50001;
          errorMessage = errorcodes.E50001;
          r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": {} };
          return callback(r);

        } else {

          getfacebookfriends(data.user.facebook.id, function(fbfriends){

            //console.log(fbfriends);
            var query = mongoose.model('WeeklyRanking').find();

            query.where('facebook_id').in(fbfriends);
            query.sort('rank');
            query.skip(startwith).limit(limitperpage).sort([[ sortwhat, sortorder]]);
            query.exec(function(err, userranks){
              r = { "errorCode": errorCode, "errorMessage": "Success", "data": { "weeklyRanking": userranks } };
              return callback(r);
            });

          });
        }
      }
    });

  } else if (this.filter=='country') {

    getuserinfo(this.user_id, function(data){
      if (validator.isNull(data.user)) {

        errorCode = 12001;
        errorMessage = errorcodes.E12001;
        r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": {} };
        return callback(r);

      } else {

        if (validator.isNull(data.user.facebook)) {

          errorCode = 50001;
          errorMessage = errorcodes.E50001;
          r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": {} };
          return callback(r);

        } else {

          var query = mongoose.model('WeeklyRanking').find();

          query.where('country').equals(data.user.facebook.locale);
          query.sort('rank');
          query.skip(startwith).limit(limitperpage).sort([[ sortwhat, sortorder]]);
          query.exec(function(err, userranks){
            r = { "errorCode": errorCode, "errorMessage": "Success", "data": { "weeklyRanking": userranks } };
            return callback(r);
          });
        }
      }
    });


  } else {
    q = this.model('WeeklyRanking').find();

    q.skip(startwith).limit(limitperpage).sort([[ sortwhat, sortorder]]);
    q.exec(function(err, ranks){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }

      if(ranks){
        errorMessage = "Success";
        data = { "weeklyRanking" : ranks };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }

};

userSchema.methods.currentranking = function(callback){

  getAllUserRanking(function(users){
    //console.log(users);

    /*users.sort(function(a, b){
      if (a.point == b.point) return 0;
      if (a.point > b.point) return -1;
      return 1;
    });*/

    var ranks = [];
    var r = 1;

    var currentRanking = mongoose.model('CurrentRanking');
    currentRanking.find().remove().exec();

    for(var i = 0; i < users.length; i++) {
      var curr_point = users[i].point;
      if (i > 0 && curr_point < prev_point) r = i + 1;
      ranks.push({rank: r, point: curr_point,user_id: users[i].uid});
      var prev_point = curr_point;
    }

    console.log('All Time Ranks Count: ' +ranks.length);

    for(var i = 0; i < ranks.length; i++) {
      //console.log(ranks[i]);

      getuserinfobyrank(ranks[i], function(data){

        if (data.user.joinWay == 2) {
          var userphoto = data.user.facebook.imgUrl;
        } else if (data.user.photoURL) {
          var userphoto = data.user.photoURL;
        } else {
          var userphoto = "";
        }

        if (validator.isNull(data.user.userLevelDefine_id)) {
          var userLevelDefineID = '001';
        } else {
          var userLevelDefineID = data.user.userLevelDefine_id;
        }

        var datas = {
            "rank": data.rank.rank,
            "user_id": data.rank.user_id,
            "accuPnt": data.rank.point,
            "email": data.user.email,
            "photoURL": userphoto,
            "name": data.user.name,
            "userLevelDefine_id": userLevelDefineID,
            "country": data.user.country,
            "facebook_id": data.user.facebook.id
          };
        var current = new currentRanking(datas);

        // Submit to the DB
        current.save(function (err) {
          if (err) {
            console.log("Error: "+err);
          } else {
            //console.log(current);
          }
        });

      });
    }

    var date = new Date();
    var datetimenow = date.getFullYear()+'-'+ date.getMonth()+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
    return callback('Cron Current Ranking END '+datetimenow);

  });

};

userSchema.methods.weeklyranking = function(callback){

  var today = new Date();
  var dateend = today.getTime();
  var minussevendays = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
  var datestart = minussevendays.getTime();

  var dend = today.getFullYear()+'-'+ today.getMonth()+'-'+today.getDate()+' '+today.getHours()+':'+today.getMinutes()+':'+today.getSeconds();
  var dstart = minussevendays.getFullYear()+'-'+ minussevendays.getMonth()+'-'+minussevendays.getDate()+' '+minussevendays.getHours()+':'+minussevendays.getMinutes()+':'+minussevendays.getSeconds();

  getAllUsersWeeklyRanking(datestart,dateend,function(users){
    console.log('START: '+dstart);
    console.log('END: '+dend);
    //console.log(users);

    users.sort(function(a, b){
      if (a.point == b.point) return 0;
      if (a.point > b.point) return -1;
      return 1;
    });

    var weeklyranks = [];
    var r = 1;

    var weeklyRanking = mongoose.model('WeeklyRanking');
    weeklyRanking.find().remove().exec();

    for(var i = 0; i < users.length; i++) {
      var curr_point = users[i].point;
      if (i > 0 && curr_point < prev_point) r = i + 1;
      weeklyranks.push({rank: r, point: curr_point,user_id: users[i].uid});
      var prev_point = curr_point;
    }

    console.log('Weekly Ranks Count: ' +weeklyranks.length);

    for(var i = 0; i < weeklyranks.length; i++) {

      getuserinfobyrank(weeklyranks[i], function(data){

        if (data.user.joinWay == 2) {
          var userphoto = data.user.facebook.imgUrl;
        } else if (data.user.photoURL) {
          var userphoto = data.user.photoURL;
        } else {
          var userphoto;
        }

        if (validator.isNull(data.user.userLevelDefine_id)) {
          var userLevelDefineID = '001';
        } else {
          var userLevelDefineID = data.user.userLevelDefine_id;
        }

        var datas = {
            "rank": data.rank.rank,
            "user_id": data.rank.user_id,
            "accuPnt": data.rank.point,
            "email": data.user.email,
            "photoURL": userphoto,
            "name": data.user.name,
            "userLevelDefine_id": userLevelDefineID,
            "country": data.user.country,
            "facebook_id": data.user.facebook.id
          };
        var weekly = new weeklyRanking(datas);

        // Submit to the DB
        weekly.save(function (err) {
          if (err) {
            console.log("Error: "+err);
          } else {
            //console.log(weekly);
          }
        });

      });
    }

    var date = new Date();
    var datetimenow = date.getFullYear()+'-'+ date.getMonth()+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
    return callback('Cron Weekly Ranking END '+datetimenow);
  });

};

var structureQuizSchema = new Schema({
	structure_id : String,
	structure_index : String,
  lecture_id : String,
  user_id : String
}, { collection: 'structure_quiz', versionKey: false });

function getuserLevelDefine(userLevelDefine_id, cb){
  var query = mongoose.model('UserLevelDefine').findOne({'_id': userLevelDefine_id});

  query.exec(function(err, userLevelDefine){
    return cb(userLevelDefine);
  });
}

function getuserpersonalranking(user_id, cb){
  var query = mongoose.model('CurrentRanking').findOne({'user_id': user_id});

  query.exec(function(err, userRank){
    return cb(userRank);
  });
}

function getuserpersonalweeklyranking(user_id, cb){
  var query = mongoose.model('WeeklyRanking').findOne({'user_id': user_id});

  query.exec(function(err, userRank){
    return cb(userRank);
  });
}

function getuserinfo(user_id, cb){
  var query = mongoose.model('User').findOne({'_id': user_id,'joinStatus.status':1});

  query.exec(function(err, user){
    return cb({"user":user});
  });
}

function getuserinfobyrank(rank, cb){
  var query = mongoose.model('User').findOne({'_id': rank.user_id,'joinStatus.status':1});

  query.exec(function(err, user){
    if (user) {
      return cb({"user":user,"rank":rank});
    }
  });
}

function getAllUserRanking(cb){
  var query = mongoose.model('User').find({'joinStatus.status':1});
  var x = new Array();

  query.exists('accuPnt', true);
  query.select('_id email name accuPnt');
  query.sort([[ 'accuPnt', 'descending']]);
  query.exec(function(err, users){

    for (var key in users) {
      if (users.hasOwnProperty(key)) {
        x[key] = {
          uid: users[key]._id,
          point: users[key].accuPnt
        };
      }
    }

    return cb(x);
  });
}

function getAllUsersWeeklyRanking(datestart,dateend,cb){
  var point = mongoose.model('Point');
  var x = new Array();

  point.aggregate(
    { $match: { obtain_date :{$gte: datestart, $lte: dateend}}},
    { $group: {
      '_id': '$user_id',
      'point': { '$sum' : '$pnt'}
    }},function(err, users){

    for (var key in users) {
      if (users.hasOwnProperty(key)) {
        x[key] = {
          uid: users[key]._id,
          point: users[key].point
        };
      }
    }

    return cb(x);
  });
}

function getfacebookfriends(fb_id,cb) {

  var x = new Array();
  var i = 0;

  facebook.api('/'+fb_id+'/friends', function(data, res) {
    for (var key in data) {
      var obj = data[key];
      for (var prop in obj) {
        if(obj.hasOwnProperty(prop)){
          if (obj[prop].id) {
            x[i] = obj[prop].id;
            i = i+1;
          }
        }
      }
    }
    x[i] = fb_id;
    return cb(x);
  });
}

function getlatestappversion (cb) {

  var query = mongoose.model('AppInfo').findOne();

  query.sort([[ 'version', 'descending']]);
  query.exec(function(err, appVer){
    return cb(appVer);
  });
}

function getPointCaseCollection (cb) {

  var query = mongoose.model('PointCase').find();

  query.exec(function(err, pointCase){
    return cb(pointCase);
  });
}

function getuserrankneighbor(userid,user_rank,limitperpage,sortwhat,sortorder,cb) {

  if (sortorder=='descending'){
    var query = mongoose.model('CurrentRanking').find({user_id: { '$ne': userid }, rank: { '$lte': user_rank }});
  } else {
    var query = mongoose.model('CurrentRanking').find({user_id: { '$ne': userid }, rank: { '$gte': user_rank }});
  }

  //query.select('_id rank user_id accuPnt email photoURL name userLevelDefine_id country facebook_id');
  query.limit(limitperpage);
  query.sort([[ sortwhat, sortorder ]]);
  query.exec(function(err, ranks){
    return cb(ranks);
  });
}




var User = mongoose.model('User', userSchema);
exports.User = User;

mongoose.model('Point', pointSchema);
mongoose.model('UserLevelDefine', userLevelDefineSchema);
mongoose.model('PointCase', pointCaseSchema);
mongoose.model('CurrentRanking', currentRankingSchema);
mongoose.model('WeeklyRanking', weeklyRankingSchema);
mongoose.model('AppInfo', appInfoSchema);
mongoose.model('Sales', salesSchema);
mongoose.model('SalesLog', salesLogSchema);
mongoose.model('StructureQuiz', structureQuizSchema);