global.errorcodes = {

  //missing parameters errors
  E11001: "Missing user_id parameter",
  E11002: "Missing content_id parameter",
  E11003: "Missing comment parameter",
  E11004: "Missing comment_id parameter",
  E11005: "Missing master_user_id parameter",
  E11006: "Missing announcement_id parameter",


  //invalid parameter errors
  E12001: "Invalid user_id parameter",
  E12022: "Invalid version parameter",

  //others
  E99999: "Unknown Error",
  E99998: "Empty Database Result",
  E99997: "Database Select Error",
  E99996: "Database Add Error",
  E99995: "Database Update Error",
  E99994: "HTTPS connection only. Retry method using a secure connection (via SSL)",
};