var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator');

var movieSchema = new Schema({
  createAdmin: Number,
  desc: { ko: String, jp: String, cn: String, en: String },
  duration: Number,
  lecture_id: String,
  likeCount: Number,
  modified: Number,
  modifyAdmin: String,
  movieURL: String,
  structure_id: String,
  thumbURL: String,
  title: { ko: String, jp: String, cn: String, en: String },
  type: Number
}, { collection: 'movie', versionKey: false });

var movieRecordSchema = new Schema({
  movie: Schema.Types.Mixed,
  playTime: Number,
  status: Number,
  user_id: String
}, { collection: 'movieRecord', versionKey: false });

movieSchema.methods.getmovielecture = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};
  
  if (validator.isNull(this.lecture_id) && validator.isNull(this.structure_id)){
    
    if (validator.isNull(this.lecture_id)) {
      errorCode = 11003;
      errorMessage = errorcodes.E11003;
    } else {
      errorCode = 11017;
      errorMessage = errorcodes.E11017;
    }
  } else if (validator.isNull(this.type)){
    errorCode = 11018;
    errorMessage = errorcodes.E11018;
  }
  
  if (errorCode > 0) {
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  }

  var q;

  if( !validator.isNull(this.lecture_id) ){
    q = this.model('Movie').find({'lecture_id': this.lecture_id,'type': this.type});
  } else {
    q = this.model('Movie').find({'structure_id': this.structure_id,'type': this.type});
  }

  q.exec(function(err, movie){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(movie){
      errorMessage = "Success";
      data = { "movie" : movie };
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });
};

movieSchema.methods.getmovielecturebyid = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this._id)){
    r = {
      "errorCode": 11010,
      "errorMessage": errorcodes.E11010,
      "data": data
    };
    return callback(r);
  }

  var q;
  q = this.model('Movie').findOne({'_id': this._id});

  q.exec(function(err, movie){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(movie){
      errorMessage = "Success";
      data = movie;
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });
};

mongoose.model('Movie', movieSchema);
mongoose.model('MovieRecord', movieRecordSchema);