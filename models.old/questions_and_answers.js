var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator'),
  random = require('mongoose-random'),
  mongodb = require('mongodb');

var questionSchema = new Schema({
  answerDesc : String,
  answerItems : [{ answerId : Number, answerText : String, isCorrect : Number }],
  createAdmin : Number,
  lang : String,
  lecture_id : String,
  modified : Number,
  modifiedAdmin : String,
  questionTxt : String,
  questionType : Number,
  seq : Number,
  structure_id : String,
  studyGrade : Number
}, { collection: 'question', versionKey: false });

questionSchema.plugin(random);

questionSchema.methods.getQuiz = function(question_type, user_id, structure_id, cb){

  getCorrectQuestionDatas(user_id, question_type, structure_id, function(correct_question_count, correct_question_ids, studyGrade){
    getQuestion(question_type, structure_id, studyGrade, correct_question_ids, function(total_question_count, selected_question){
      if(0 == total_question_count){
        return cb({ "errorCode": 90, "errorMessage":"No data found.", "data": {} });
      } else {
        var is_done = total_question_count == correct_question_count ? 1 : 0;
        var data = { "question": selected_question, "total_question_count": total_question_count, "correct_answer_count": correct_question_count, "is_done": is_done, "studyGrade": studyGrade };
        return cb({ "errorCode": 0, "errorMessage":"Success", "data": data });
      }
    });
  });
};

questionSchema.methods.answerQuiz = function(quiz_id, answer_id, user_id, question_type, lecture_id, cb){
  var q = {};
  if( 3 == question_type ){
    q = this.model('Question').findOne({ 'questionType': question_type, 'lecture_id': lecture_id });
  } else {
    q = this.model('Question').findOne({ '_id': quiz_id });
  }
  q.exec(function(err, question){
    if(err){
      return cb({ "errorCode": 92, "errorMessage":"No data found", "data": {} });
    }

    checkQuestionIfAnswered(question._id, user_id, function(count){
      if(count > 0){
        return cb({ "errorCode": 93, "errorMessage":"Already answered.", "data": {} });
      } else {
        structure_id = question.structure_id;

        if(question){
          getCorrectQuestionDatas(user_id, question.questionType, structure_id, function(correct_question_count, correct_question_ids, studyGrade){
            var is_correct = 0;
            if( 3 == question_type ){
              insertCorrectQuiz(question, user_id);
              is_correct = 1;
              correct_question_count += 1;
            } else {
              question.answerItems.forEach(function(answerItem){
                if(answer_id == answerItem.answerId && 1 == answerItem.isCorrect){
                  is_correct = 1;
                  correct_question_count += 1;
                  insertCorrectQuiz(question, user_id);
                }
              });
            }

            getQuestionCount(question.questionType, structure_id, question.studyGrade, lecture_id, function(total_question_count, maxStudyGrade){
              var is_grade_clear = 0;
              var is_all_grades_clear = 0;
              if(correct_question_count == total_question_count){
                is_grade_clear = 1;
                if(maxStudyGrade == question.studyGrade){
                  is_all_grades_clear = 1;
                }
              }
              return cb({ "errorCode": 0, "errorMessage":"Success", "data": { "is_correct": is_correct, "correct_answer_count": correct_question_count, "total_question_count": total_question_count, "is_grade_clear": is_grade_clear, "is_all_grades_clear": is_all_grades_clear, "answer_id": answer_id } });
            });
          });
        } else {
          return cb({ "errorCode": 93, "errorMessage":"Question not found.", "data": {} });
        }
      }
    });
  });
};

mongoose.model('Question', questionSchema);

var schoolGradeSchema = new Schema({
  studyGrade : Number,
  gradeName : { ko: String, jp: String, cn: String, en: String }
}, { collection: 'schoolGrade', versionKey: false });

mongoose.model('SchoolGrade', schoolGradeSchema);

var pubGradeSchema = new Schema({
  lecture_id: String,
  studyGrade : Number,
  publisher_id: String
}, { collection: 'pubGrade', versionKey: false });

mongoose.model('PubGrade', pubGradeSchema);

var correctQuestionSchema = new Schema({
  question: Schema.Types.Mixed,
  user_id: String
}, { collection: 'correctQuestion', versionKey: false });

mongoose.model('CorrectQuestion', correctQuestionSchema);

function getCorrectQuestionDatas(user_id, question_type, structure_id, cb){

  getStudyGradeAndCorrectQuestionCount(user_id, question_type, structure_id, function(studyGrade, correct_question_count){
    var query = mongoose.model('CorrectQuestion').find({
      "user_id" : user_id,
      "question.questionType": question_type,
      "question.structure_id": structure_id,
      "question.studyGrade": studyGrade
    });

    query.exec(function(err, correct_questions){
      var correct_question_ids = [];
      if(correct_questions){
        correct_question_ids = correct_questions.map(function(i){
          return i.question._id;
        });
      }

      return cb(correct_question_count, correct_question_ids, studyGrade);
    });
  });
}

function getStudyGradeAndCorrectQuestionCount(user_id, question_type, structure_id, cb){
  var query = mongoose.model('CorrectQuestion').find({
    "user_id" : user_id,
    "question.questionType": question_type,
    "question.structure_id": structure_id
  });
  query.limit(1).sort('-question.studyGrade').exec(function(err, correct_question){
    var studyGrade = correct_question && correct_question.length > 0 ? correct_question[0].question.studyGrade : 1 ;
    getQuestionCount(question_type, structure_id, studyGrade, null, function(total_question_count){
      getCorrectQuestionCount(user_id, question_type, structure_id, studyGrade, function(correct_question_count){
        if(correct_question_count >= total_question_count){
          studyGrade += 1;
          getCorrectQuestionCount(user_id, question_type, structure_id, studyGrade, function(correct_question_count){
            return cb(studyGrade, correct_question_count);
          });
        } else {
          return cb(studyGrade, correct_question_count);
        }
      });
    });
  });
}

function getCorrectQuestionCount(user_id, question_type, structure_id, studyGrade, cb){
  var query = mongoose.model('CorrectQuestion').find({
    "user_id" : user_id,
    "question.questionType": question_type,
    "question.structure_id": structure_id,
    "question.studyGrade": studyGrade
  });
  query.exec(function(err, correct_questions){
    var correct_question_count = correct_questions ? correct_questions.length : 0;
    return cb(correct_question_count);
  });
}

function getQuestion(question_type, structure_id, studyGrade, correct_question_ids, cb){
  var query = mongoose.model('Question').find({ 'questionType' : question_type, 'structure_id' : structure_id, 'studyGrade' : studyGrade });

  query.exec(function(err, questions){
    query.where('_id').nin(correct_question_ids).exec(function(err, nquestions){
      var selected_question = nquestions[Math.floor(Math.random()*nquestions.length)];
      return cb(questions.length, selected_question);
    });
  });
}

function getQuestionCount(question_type, structure_id, studyGrade, lecture_id, cb){
  var p = {
    'questionType' : question_type,
    'structure_id' : structure_id,
    'studyGrade'   : studyGrade
  };

  if(3==question_type && lecture_id){
    p.lecture_id = lecture_id;
  }

  var question = mongoose.model('Question');

  question.find(p, function(err, questions){
    var c = questions ? questions.length : 0 ;
    question.findOne({'questionType' : question_type, 'structure_id' : structure_id}).limit(1).sort('-studyGrade').exec(function(err, doc){
      var studyGrade = doc ? doc.studyGrade : 1 ;
      return cb(c, studyGrade);
    });
  });
}

function insertCorrectQuiz(question, user_id){
  var CorrectQuestion = mongoose.model('CorrectQuestion');
  var correctQuestion = new CorrectQuestion({ "question": question, "user_id": user_id });
  correctQuestion.save(function(err){});
}

function checkQuestionIfAnswered(id, user_id, cb){
  mongoose.model('CorrectQuestion').find({'question._id' : id, 'user_id': user_id}, function(err, doc){
    return cb(doc.length);
  });
}

function getStudyGrade(structure_id, user_id, cb){
  mongoose.model('CorrectQuestion').findOne({'question.structure_id':structure_id, 'user_id': user_id}).sort('-question.studyGrade').select('question.studyGrade').exec(cb);
}