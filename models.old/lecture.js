var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator');

var lectureSchema = new Schema({
  lecture_id: String,
  caption: {exists: Number, text: { ko: String, jp: String, cn: String, en: String, ar: String }},
  example: Schema.Types.Mixed,
  isLastNode: Number,
  example: Schema.Types.Mixed, //[{ ko: String, jp: String, cn: String, en: String, ar: String },{ ko: String, jp: String, cn: String, en: String, ar: String }]
  //structure_id: { type: Schema.Types.ObjectId, ref: 'structure' },
  structure_id: String,
  angle: Number,
  parent: Number,
  child1: Number,
  child2: Number,
  child3: Number,
  child4: Number,
  child5: Number,
  child6: Number,
  child7: Number,
  child8: Number,
  pos: Number,
  treenum: Number,
  color: Number,
  difficulty: Number,
  frequency: Number,
  grade: Number,
  importance: Number,
  createAdmin : String,
  modifyAdmin : String,
  modified : Number,

  filter: String,
  limit: Number,
  page: Number,
  sort: String,
  order: String
}, { collection: 'lecture', versionKey: false });


lectureSchema.methods.getlectures = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  limitperpage = this.limit;
  pagenumber = this.page;
  sortwhat = this.sort;
  sortorder = this.order;

  var startwith = (pagenumber * limitperpage) - limitperpage;

  
  var q;
  q = this.model('Lecture').find();
  q.sort(this.sort);
  q.skip(startwith).limit(limitperpage).sort([[ sortwhat, sortorder]]);

  q.exec(function(err, lecture){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(lecture){
      errorMessage = "Success";
      data = { "lecture" : lecture };
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });
};

lectureSchema.methods.synclectures = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  var q;
  q = this.model('Lecture').find({'modified': { '$gt': this.modified }});

  q.exec(function(err, lecture){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(lecture){
      errorMessage = "Success";
      data = { "lecture" : lecture };
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });
};

mongoose.model('Lecture', lectureSchema);