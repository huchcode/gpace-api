var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator');

var userExampleSchema = new Schema({
  commentCount: { type: Number, default: 0 },
  user_id: String,
  creator: Schema.Types.Mixed,
  imgURL: String,
  desc: String,
  lastCommented: Number,
  lastLiked: Number,
  lecture_id: String,
  likeCount: { type: Number, default: 0 },
  modified: Number,
  sentence: String,
  status: { type: Number, default: 1 },
  subject: String,

  limit: Number,
  page: Number,
  sort: String,
  order: String

}, { collection: 'userExample', versionKey: false });

var likeExmpSchema = new Schema({
  modified: Number,
  status: { type: Number, default: 1 },
  user_id: String,
  userExample_id: String,
  userName: String
}, { collection: 'likeExmp', versionKey: false });


var userCommentSchema = new Schema({
  comment: String,
  commentor: Schema.Types.Mixed,
  lastLiked: Number,
  likeCount: { type: Number, default: 0 },
  modified: Number,
  status: { type: Number, default: 1 },
  toWhom: String,
  userExample_id: String,
}, { collection: 'userComment', versionKey: false });

var likeExmpCommSchema = new Schema({
  modified: Number,
  status: { type: Number, default: 1 },
  user_id: String,
  userComment_id: String,
  userName: String
}, { collection: 'likeExmpComm', versionKey: false });


userExampleSchema.methods.getuserexample = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  console.log(this);

  if (validator.isNull(this._id)){
    errorCode = 11004;
    errorMessage = errorcodes.E11004;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q = this.model('UserExample').find({'_id': this._id});
    q.exec(function(err, userExample){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }

      if(userExample){
        errorMessage = "Success";
        data = { "userExample" : userExample };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

userExampleSchema.methods.getuserexamples = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  limitperpage = this.limit;
  pagenumber = this.page;
  sortwhat = this.sort;
  sortorder = this.order;

  var startwith = (pagenumber * limitperpage) - limitperpage;

  console.log(this);

  if (validator.isNull(this.lecture_id)){
    errorCode = 11003;
    errorMessage = errorcodes.E11003;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);

  //will remove the user_id as a requirement as per YongGyuPark's request - 2014-08-06 09:29
  /*} else if (validator.isNull(this.user_id)){
    errorCode = 11001;
    errorMessage = errorcodes.E11001;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);*/

  } else {

    var q;
    if (validator.isNull(this.user_id)){
      q = this.model('UserExample').find({'lecture_id': this.lecture_id,'status': 1});
    } else {
      var ObjectId = require('mongoose').Types.ObjectId;
      var uid = new ObjectId(this.user_id);

      q = this.model('UserExample').find({'creator.data.user._id':uid,'lecture_id': this.lecture_id,'status': 1});
    }

    q.skip(startwith).limit(limitperpage).sort([[ sortwhat, sortorder]]);
    q.exec(function(err, userExample){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }

      if(userExample){
        errorMessage = "Success";
        data = { "userExample" : userExample };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

userExampleSchema.methods.deleteuserexample = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this._id)){
    errorCode = 11004;
    errorMessage = errorcodes.E11004;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q;
    /*q = this.model('UserExample').findByIdAndRemove({'_id':this._id});*/
    var modified = new Date().getTime();

    var query = {'_id':this._id};
    var update = {'status':2,'modified':modified};
    var option = {};

    q = this.model('UserExample').update(query,update,option);

    q.exec(function(err, userExample){
      if(err){
        errorCode = 99995;
        errorMessage = errorcodes.E99995+'. '+err;
      }

      if(userExample){
        errorMessage = "Success";
        data = { "userExample" : query };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

userCommentSchema.methods.getuserexamplecomment = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this.userExample_id)){
    errorCode = 11004;
    errorMessage = errorcodes.E11004;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q;
    q = this.model('UserComment').find({'userExample_id':this.userExample_id,'status':1});

    q.exec(function(err, userComment){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }

      if(userComment){
        errorMessage = "Success";
        data = { "userComment" : userComment };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

userCommentSchema.methods.deleteuserexamplecomment = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this._id)){
    errorCode = 11005;
    errorMessage = errorcodes.E11005;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q;

    var modified = new Date().getTime();

    var query = {'_id':this._id};
    var update = {'status':2,'modified':modified};
    var option = {};

    q = this.model('UserComment').update(query,update,option);

    q.exec(function(err, userComment){
      if(err){
        errorCode = 99995;
        errorMessage = errorcodes.E99995+'. '+err;
      }

      if(userComment){
        errorMessage = "Success";
        data = { "userComment" : query };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

likeExmpSchema.methods.like = function(callback){
	var user_id = this.user_id;
	var userExample_id = this.userExample_id;
	var modified = this.modified;
	var userName = this.userName;
	var LikeExmp = this.model('LikeExmp');
	var UserExample = this.model('UserExample');

	UserExample.findOne({ '_id':userExample_id }, 'likeCount lastLiked', function(err, user_exam){
		if( user_exam ){
			var q = { 'userExample_id': user_exam._id, 'status': 1, 'user_id': user_id };
			LikeExmp.findOne(q).exec(function(err, doc){
				if(doc){
					doc.status = 2;
					doc.save(function(err){
		        var lc = user_exam.likeCount > 0 ? user_exam.likeCount-1 : 0;
		        user_exam.likeCount = lc;
		        user_exam.save(function(err){
		        	var r = {
			          "errorCode":0,
			          "errorMessage":"Success",
			          "data":{ "likeExmp" : doc, "likeCount": lc }
			        };
			        return callback(r);
		        });
					});
				}	else {
					var datas = {
			      "modified" : modified,
			      "userExample_id" : user_exam._id,
			      "status" : 1,
			      "user_id" : user_id,
			      "userName" : userName
			    };
					var likeExmp = new LikeExmp(datas);
					likeExmp.save(function(err){
						user_exam.likeCount = user_exam.likeCount + 1;
						user_exam.lastLiked = modified;
						user_exam.save(function(err){
							var r = {
		          "errorCode":0,
		          "errorMessage":"Success",
		          "data":{ "likeExmp" : likeExmp, "likeCount": user_exam.likeCount }
		        };
		        return callback(r);
						});
					});
				}
			});
		}	else {
			var r = {
				"errorCode" :99996,
				"errorMessage":errorcodes.E99996,
				"data":{}
				};
			return callback(r);
		}
	});
};

likeExmpCommSchema.methods.like = function(callback){
	var user_id = this.user_id;
	var userComment_id = this.userComment_id;
	var modified = this.modified;
	var userName = this.userName;
	var LikeExmpComm = this.model('LikeExmpComm');
	var UserComment = this.model('UserComment');

	UserComment.findOne({ '_id':userComment_id }, 'likeCount lastLiked', function(err, user_comm){
		if( user_comm ){
			var q = { 'userComment_id': user_comm._id, 'status': 1, 'user_id': user_id };
			LikeExmpComm.findOne(q).exec(function(err, doc){
				if(doc){
					doc.status = 2;
					doc.save(function(err){
		        var lc = user_comm.likeCount > 0 ? user_comm.likeCount-1 : 0;
		        user_comm.likeCount = lc;
		        user_comm.save(function(err){
		        	var r = {
			          "errorCode":0,
			          "errorMessage":"Success",
			          "data":{ "likeExmpComm" : doc, "likeCount": lc }
			        };
			        return callback(r);
		        });
					});
				}	else {
					var datas = {
			      "modified" : modified,
			      "userComment_id" : user_comm._id,
			      "status" : 1,
			      "user_id" : user_id,
			      "userName" : userName
			    };
					var likeExmpComm = new LikeExmpComm(datas);
					likeExmpComm.save(function(err){
						user_comm.likeCount = user_comm.likeCount + 1;
						user_comm.lastLiked = modified;
						user_comm.save(function(err){
							var r = {
		          "errorCode":0,
		          "errorMessage":"Success",
		          "data":{ "likeExmpComm" : likeExmpComm, "likeCount": user_comm.likeCount }
		        };
		        return callback(r);
						});
					});
				}
			});
		}	else {
			var r = {
				"errorCode" :99996,
				"errorMessage":errorcodes.E99996,
				"data":{}
				};
			return callback(r);
		}
	});
};

mongoose.model('UserExample', userExampleSchema);
mongoose.model('LikeExmp', likeExmpSchema);
mongoose.model('UserComment', userCommentSchema);
mongoose.model('LikeExmpComm', likeExmpCommSchema);
