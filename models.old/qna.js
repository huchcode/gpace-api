var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('validator');

var qnaSchema = new Schema({
  commentCount: { type: Number, default: 0 },
  //content: String,
  creator: Schema.Types.Mixed,
  imgURL: String,
  lastCommented: Number,
  lastLiked: Number,
  lecture_id: String,
  user_id: String,
  likeCount: { type: Number, default: 0 },
  modified: Number,
  status: { type: Number, default: 1 },
  subject: String
}, { collection: 'qna', versionKey: false });

var likeQnaSchema = new Schema({
  modified: Number,
  qna_id: { type: Schema.Types.ObjectId, ref: 'Qna' },
  status: { type: Number, default: 1 },
  user_id: String,
  userName: String
}, { collection: 'likeQna', versionKey: false });

var qnaCommentSchema = new Schema({
  comment: String,
  commentor: Schema.Types.Mixed,
  lastLiked: Number,
  likeCount: { type: Number, default: 0 },
  modified: Number,
  qna_id: String,
  status: { type: Number, default: 1 },
  toWhom: String,
}, { collection: 'qnaComment', versionKey: false });

var likeQnaCommSchema = new Schema({
  modified: Number,
  qnaComment_id: String,
  status: { type: Number, default: 1 },
  user_id: String,
  userName: String
}, { collection: 'likeQnaComm', versionKey: false });

var qnaBegSchema = new Schema({
  checked: Number,
  fromU_id: String,
  modified: Number,
  qna: Schema.Types.Mixed,
  requested: Number,
  status: { type: Number, default: 0 },
  toU_id: String
}, { collection: 'qnaBeg', versionKey: false });


qnaSchema.methods.getqna= function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  console.log(this);

  if (validator.isNull(this._id)){
    errorCode = 11009;
    errorMessage = errorcodes.E11009;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q = this.model('Qna').find({'_id': this._id});
    q.exec(function(err, qna){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }

      if(qna){
        errorMessage = "Success";
        data = { "qna" : qna };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

qnaSchema.methods.getqnas = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  var q;

  if (validator.isNull(this.lecture_id)){
    
    var ObjectId = require('mongoose').Types.ObjectId;
    var uid = new ObjectId(this.user_id);
    var search_query = {'creator.data.user._id':uid,'status': 1} ;
    
  } else if (validator.isNull(this.user_id)){
    
    var search_query = {'lecture_id': this.lecture_id,'status': 1} ;
    
  } else {
    
    var ObjectId = require('mongoose').Types.ObjectId;
    var uid = new ObjectId(this.user_id);

    var search_query = {'creator.data.user._id':uid,'lecture_id': this.lecture_id,'status': 1} ;
  }
  q = this.model('Qna').find(search_query);

  q.exec(function(err, qna){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(qna){
      errorMessage = "Success";
      data = { "qna" : qna };
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });

};

qnaSchema.methods.getqnabyid = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this._id)){
    r = {
      "errorCode": 11009,
      "errorMessage": errorcodes.E11009,
      "data": data
    };
    return callback(r);
  }

  var q;
  q = this.model('Qna').findOne({'_id': this._id});

  q.exec(function(err, qna){
    if(err){
      errorCode = 99997;
      errorMessage = errorcodes.E99997+'. '+err;
    }

    if(qna){
      errorMessage = "Success";
      data = qna;
    } else {
      errorCode = 99998;
      errorMessage = errorcodes.E99998;
    }

    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  });
};

qnaSchema.methods.deleteqna = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this._id)){
    errorCode = 11009;
    errorMessage = errorcodes.E11009;
    r = {"errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var modified = new Date().getTime();

    var query = {'_id':this._id};
    var update = {'status':2,'modified':modified};
    var option = {};

    var q;
    q = this.model('Qna').update(query,update,option);

    q.exec(function(err, qna){
      if(err){
        errorCode = 99995;
        errorMessage = errorcodes.E99995+'. '+err;
      }

      if(qna){
        
        errorMessage = "Success";
        data = { "qna" : qna };
        console.log(qna);
        
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = {
        "errorCode": errorCode,
        "errorMessage": errorMessage,
        "data": data
      };

      return callback(r);
    });
  }
};

qnaCommentSchema.methods.getqnacomment = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this.qna_id)){
    errorCode = 11009;
    errorMessage = errorcodes.E11009;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q;
    q = this.model('QnaComment').find({'qna_id':this.qna_id});

    q.exec(function(err, qnaComment){
      if(err){
        errorCode = 99997;
        errorMessage = errorcodes.E99997+'. '+err;
      }

      if(qnaComment){
        errorMessage = "Success";
        data = { "qnaComment" : qnaComment };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
      return callback(r);
    });
  }
};

qnaCommentSchema.methods.deleteqnacomment = function(callback) {
  var errorCode = 0;
  var errorMessage = "";
  var data = {};
  var r = {};

  if (validator.isNull(this._id)){
    errorCode = 11016;
    errorMessage = errorcodes.E11016;
    r = { "errorCode": errorCode, "errorMessage": errorMessage, "data": data };
    return callback(r);
  } else {
    var q;

    var modified = new Date().getTime();

    var query = {'_id':this._id};
    var update = {'status':2,'modified':modified};
    var option = {};

    q = this.model('QnaComment').update(query,update,option);

    q.exec(function(err, qnaComment){
      if(err){
        errorCode = 99995;
        errorMessage = errorcodes.E99995+'. '+err;
      }

      if(qnaComment){
        errorMessage = "Success";
        data = { "qnaComment" : qnaComment };
      } else {
        errorCode = 99998;
        errorMessage = errorcodes.E99998;
      }

      r = {"errorCode": errorCode, "errorMessage": errorMessage, "data": data};
      return callback(r);
    });
  }
};

likeQnaSchema.methods.like = function(callback){
	var user_id = this.user_id;
	var qna_id = this.qna_id;
	var modified = this.modified;
	var userName = this.userName;
	var LikeQna = this.model('LikeQna');
	var Qna = this.model('Qna');

	Qna.findOne({ '_id':qna_id }, 'likeCount lastLiked', function(err, qna){
		if( qna ){
			var q = { 'qna_id': qna._id, 'status': 1, 'user_id': user_id };
			LikeQna.findOne(q).exec(function(err, doc){
				if(doc){
					doc.status = 2;
					doc.save(function(err){
		        var lc = qna.likeCount > 0 ? qna.likeCount-1 : 0;
		        qna.likeCount = lc;
		        qna.save(function(err){
		        	var r = {
			          "errorCode":0,
			          "errorMessage":"Success",
			          "data":{ "likeQna" : doc, "likeCount": lc }
			        };
			        return callback(r);
		        });
					});
				}	else {
					var likeQna = new LikeQna({
						"modified" : modified,
						"qna_id" : qna_id,
						"status" : 1,
						"user_id" : user_id,
						"userName" : userName
						});
					likeQna.save(function(err){
						qna.likeCount = qna.likeCount + 1;
						qna.lastLiked = modified;
						qna.save(function(err){
							var r = {
		          "errorCode":0,
		          "errorMessage":"Success",
		          "data":{ "likeQna" : likeQna, "likeCount": qna.likeCount }
		        };
		        return callback(r);
						});
					});
				}
			});
		}	else {
			var r = {
				"errorCode" :99996,
				"errorMessage":errorcodes.E99996,
				"data":{}
				};
			return callback(r);
		}
	});
};

likeQnaCommSchema.methods.like = function(callback){
	var user_id = this.user_id;
	var qnaComment_id = this.qnaComment_id;
	var modified = this.modified;
	var userName = this.userName;
	var LikeQnaComm = this.model('LikeQnaComm');
	var QnaComment = this.model('QnaComment');

	QnaComment.findOne({ '_id':qnaComment_id }, 'likeCount lastLiked', function(err, qna_com){
		if( qna_com ){
			var q = { 'qnaComment_id': qna_com._id, 'status': 1, 'user_id': user_id };
			LikeQnaComm.findOne(q).exec(function(err, doc){
				if(doc){
					doc.status = 2;
					doc.save(function(err){
		        var lc = qna_com.likeCount > 0 ? qna_com.likeCount-1 : 0;
		        qna_com.likeCount = lc;
		        qna_com.save(function(err){
		        	var r = {
			          "errorCode":0,
			          "errorMessage":"Success",
			          "data":{ "likeQnaComm" : doc, "likeCount": lc }
			        };
			        return callback(r);
		        });
					});
				}	else {
					var datas = {
			      "modified" : modified,
			      "qnaComment_id" : qnaComment_id,
			      "status" : 1,
			      "user_id" : user_id,
			      "userName" : userName
			    };
					var likeQnaComm = new LikeQnaComm(datas);
					likeQnaComm.save(function(err){
						qna_com.likeCount = qna_com.likeCount + 1;
						qna_com.lastLiked = modified;
						qna_com.save(function(err){
							var r = {
		          "errorCode":0,
		          "errorMessage":"Success",
		          "data":{ "likeQnaComm" : likeQnaComm, "likeCount": qna_com.likeCount }
		        };
		        return callback(r);
						});
					});
				}
			});
		}	else {
			var r = {
				"errorCode" :99996,
				"errorMessage":errorcodes.E99996,
				"data":{}
				};
			return callback(r);
		}
	});
};

mongoose.model('Qna', qnaSchema);
mongoose.model('LikeQna', likeQnaSchema);
mongoose.model('QnaComment', qnaCommentSchema);
mongoose.model('LikeQnaComm', likeQnaCommSchema);
mongoose.model('QnaBeg', qnaBegSchema);
